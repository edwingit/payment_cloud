<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
//9/9/2020 edwin
$router->get('/health', function () use ($router) {
    echo json_encode([
        'code'      =>  200,
        'status'    =>  'ok'
    ]);
});

$router->get('/version', function () use ($router) {
    echo json_encode([
        'code'      =>  200,
        'status'    =>  'ok',
        'versions'  =>  '2020-12-10'
    ]);
});