<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_logs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('origin_ip', 255)->nullable(false);
            $table->integer('origin_merchant_id')->nullable(false);
            $table->tinyInteger('app_id')->nullable(false);
            $table->string('auth_header', 255)->nullable(false);
            $table->string('message_type', 255)->nullable(false);
            $table->integer('response_code')->nullable(false);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable(false);
            $table->index('app_id');
            $table->index('response_code');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_logs');
    }
}
