<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('order_id')->nullable(true)->default(0);
            $table->integer('customer_id')->nullable(true)->default(0);
            $table->decimal('total_amount', 10, 2)->nullable(false);
            $table->string('table_no', 255)->nullable(true);
            $table->string('currency_code', 255)->nullable(true);
            $table->text('msg_id')->nullable(true);
            $table->string('txn_id', 255)->nullable(true);
            $table->tinyInteger('status')->nullable(true)->comment('1 - Success Generate, 2 - Success Reversal  3 - Sucess Refund, 4 - Success Query');
            $table->text('request')->nullable(true);
            $table->text('response')->nullable(true);
            $table->integer('merchant_id')->nullable(true);
            $table->text('client_info')->nullable(true);
            $table->tinyInteger('op_type')->nullable(true);
            $table->tinyInteger('app_id')->nullable(true);
            $table->dateTime('txn_date')->nullable(true);
            $table->index('order_id');
            $table->index('merchant_id');
            $table->index('status');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
