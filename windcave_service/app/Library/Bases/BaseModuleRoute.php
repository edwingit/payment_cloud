<?php

namespace App\Library\Bases;

use Laravel\Lumen\Application;

abstract class BaseModuleRoute
{
    protected $controller_ns;
    protected $route_prefix;

    const V1_GLOBAL_PREFIX = 'v1';

    abstract public function bind(Application $app);
}
