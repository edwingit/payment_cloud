<?php
	
namespace App\Library\Bases;

abstract class BaseProcessor
{
    protected $error;
    protected $error_code;
    protected $output;
    protected $message_type;
    protected $error_messages;
    protected $http_code;
    protected $response_code;
    protected $result;

    abstract function process($data, $model);

    public function getError()
    {
        return $this->error;
    }
    
    public function getResult()
    {
        return $this->result;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function getErrorCode()
    {
        return $this->error_code;
    }

    public function getHttpCode()
    {
        $http_code = 400;

        if(!in_array($this->http_code, $this->getHttpCodes()))
        {
                $this->http_code = $http_code;
        }

        return $this->http_code;
    }

    public function getMessageType()
    {
        return $this->message_type;
    }

    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    public function getResponseCode()
    {
        return $this->response_code;
    }

    public function getHttpCodes()
    {
        return [
                100,
                101,
                102,
                200,
                201,
                202,
                203,
                204,
                205,
                206,
                207,
                208,
                226,
                300,
                301,
                302,
                303,
                304,
                305,
                306,
                307,
                308,
                400,
                401,
                402,
                403,
                404,
                405,
                406,
                407,
                408,
                409,
                410,
                411,
                412,
                413,
                414,
                415,
                416,
                417,
                418,
                419,
                420,
                420,
                422,
                423,
                424,
                424,
                425,
                426,
                428,
                429,
                431,
                444,
                449,
                450,
                451,
                451,
                494,
                495,
                496,
                497,
                499,
                500,
                501,
                502,
                503,
                504,
                505,
                506,
                507,
                508,
                509,
                510,
                511,
                598,
                599
        ];
    }
}