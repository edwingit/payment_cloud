<?php
	
	namespace App\lib;
	
	use App\lib\BaseRepository;

	use Validator;
	
	class BasePaymentValidatorRepository extends BaseRepository
	{
		public function __construct()
        {
            parent::__construct();
        }

		public function validate()
		{
			$this->setValidationData();
			$this->setValidationRules();

			$validation = Validator::make($this->data, $this->rules);

			if($validation->fails())
			{
				$this->errors = $validation->errors();
				return false;
			}

			return true;
		}
	}