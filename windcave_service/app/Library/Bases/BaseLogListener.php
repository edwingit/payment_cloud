<?php

namespace App\Library\Bases; 

use App\Models\LogRequestPayment;

use App\Models\Payment;

abstract class BaseLogListener
{
    protected function getPaymentId($name)
    {
        $model = new Payment();
        $data = $model->getPaymentId($name);
        return $data;
    }

    abstract public function handle(LogEventInterface $log);
}