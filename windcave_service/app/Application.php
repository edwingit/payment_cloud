<?php

namespace App;

use Laravel\Lumen\Application as LumenApplication;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Application extends LumenApplication
{
	protected function registerLogBindings()
    {
        $this->singleton('Psr\Log\LoggerInterface', function () {
            return new Logger('lumen', $this->getMonologHandler());
        });
    }

    protected function getMonologHandler()
    {
    	$handlers = [];
    	$handlers[] = (new StreamHandler(storage_path('logs/tabs_dev_lumen.log'), Logger::DEBUG))->setFormatter(new LineFormatter(null, null, true, true));

        return $handlers;
    }
}