<?php 

    namespace App\Api\v1\Models;

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Apps extends Model
    {
        public $table = 'apps';
        public $timestamps = false;

        public function getByName($name, $status)
        {
            return $this->where("name", $name)
                        ->where("is_active", $status)
                        ->get();
        }
    }