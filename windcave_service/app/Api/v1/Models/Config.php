<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Config extends Model
    {
        public $table = 'configs';
        public $timestamps = false;
        
        public function getAll($merchant_id)
        {
            
            return $this->where("merchant_id", $merchant_id)->where("is_active", 1)->get();
        }
        
        public function getByTerminal($merchant_id,$terminal)
        {
            return $this->where("merchant_id", $merchant_id)->where("terminal_id", $terminal)->get();
        }
    }