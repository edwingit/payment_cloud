<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Log extends Model
    {
        public $table = "logs";
        public $timestamps = false;

        public function saveData($data)
        {
            $this->order_id = $data["order_id"];
            $this->customer_id = $data["customer_id"];
            $this->total_amount = $data["total_amount"];
            $this->table_no = $data["table_no"];
            $this->currency_code = $data["currency_code"];
            $this->msg_id = $data["msg_id"];
            $this->txn_id = $data["txn_id"];
            $this->status = $data["status"];
            $this->merchant_id = $data["merchant_id"];
            $this->txn_date = date("Y-m-d h:i:s");
            $this->op_type = $data["op_type"];
            $this->app_id = $data["app_id"];
            $this->client_info = $data["client_info"];
            $this->request = $data["request"];
            $this->response = $data["response"];
            $this->save();

            return $this->id;
        }

        public function getLogByTxnId($txn_id, $op_type, $status, $merchant_id)
        {
            //$query = "SELECT * FROM $this->table WHERE `txn_id` = :txn_id AND type = :transaction_type AND status = :status AND merchant_id = :merchant_id";

            return $this->where("txn_id", $txn_id)
                        ->where("op_type", $op_type)
                        ->where("status", $status)
                        ->where("merchant_id", $merchant_id)
                        ->get();
        }

        public function getMsgId($msg_id)
        {
            return $this->select("msg_id")->where("msg_id", $msg_id)->get();
        }

        public function getTxnId($txn_id)
        {
            return $this->select("txn_id")->where("txn_id", $txn_id)->get();
        }

        public function getLogs($merchant_id, $start_date, $end_date)
        {
            return $this->selectRaw("txn_date, op_type, request, response")->where("merchant_id", $merchant_id)->whereRaw("txn_date BETWEEN ? AND ?", [$start_date, $end_date])->get();
        }

        public function getLogByType($merchant_id, $op_type, $start_date, $end_date)
        {
            return $this->selectRaw("order_id, total_amount, table_no, txn_date, type, request, currency_code")->where("merchant_id", $merchant_id)->where("op_type", $op_type)->whereRaw("txn_date BETWEEN ? AND ?", [$start_date, $end_date])->get();
        }
    }