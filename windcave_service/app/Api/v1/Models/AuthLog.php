<?php 

    namespace App\Api\v1\Models;

    use Illuminate\Database\Eloquent\Model;

    class AuthLog extends Model
    {
        public $table = 'auth_logs';
        public $timestamps = false;

        public function saveData($data)
        {
            $this->origin_ip = $data["origin_ip"];
            $this->origin_merchant_id = $data["origin_merchant_id"];
            $this->app_id = $data["app_id"];
            $this->auth_header = $data["auth_header"];
            $this->message_type = $data["message_type"];
            $this->response_code = $data["response_code"];
            $this->created_at = $data["created_at"];
            $this->updated_at = $data["updated_at"];

            $this->save();

            return $this->id;
        }
    }