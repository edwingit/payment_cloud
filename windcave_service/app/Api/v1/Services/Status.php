<?php
  
namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Services\Events\LogEvent;
use Spatie\ArrayToXml\ArrayToXml;

use Log;
class Status extends BaseWindcave
{
    private $payload; 
    private $unique_txn;
    private $Terminal_response;
    private $receipt;
    
    public function process($data)
    {
        $this->op_type = "GET_STATUS";

        $this->request_data = $data;

        $this->getConfig();
        
        $status = "UNDEFINED";
        $message = "";
        //convert array to xml
        //step 1
        $this->setPayload();
        $this->getPayload();
        
        Log::info("Start Transaction ...");
        Log::info("Requested Windcave :");
        Log::info($this->getPayload());
        
        $this->doRequest();
        
        //Request give response before do trx
        if(isset($this->getXmlResponse()["Complete"]) && $this->getXmlResponse()["Complete"]==1){
            
            if(isset($this->getXmlResponse()["Result"])){
                
                $this->Terminal_response = $this->getXmlResponse();
                
                $status = $this->getXmlResponse()["Result"]["RC"];
                $message = isset($this->getXmlResponse()["DL2"])?$this->getXmlResponse()["DL2"]:$this->getXmlResponse()["Result"]["RT"];
            }
        }else if(isset($this->getXmlResponse()["Complete"]) && $this->getXmlResponse()["Complete"]==0){
            $this->Terminal_response = $this->getXmlResponse();
                
            $status = $this->getXmlResponse()["DL1"];
            $message = isset($this->getXmlResponse()["DL2"])?$this->getXmlResponse()["DL2"]:$this->getXmlResponse()["Result"]["RT"];
            
        }
        
        if($this->handleResult() == false)
        {
            return false;
        }
        
        //$this->message_type = "Transaction : ".$message;
        $this->result = $status=="00"?true:false;
        $this->output = [
                "STAN"          =>  "",
                "MerchantId"    =>  isset($this->Terminal_response["Result"]["MID"])?is_array($this->Terminal_response["Result"]["MID"])?"":"'".$this->Terminal_response["Result"]["MID"]."'":"",
                "TerminalID"    =>  isset($this->Terminal_response["Result"]["TID"])?is_array($this->Terminal_response["Result"]["TID"])?"":"'".$this->Terminal_response["Result"]["TID"]."'":"",
                "BankDateTime"  =>  isset($this->Terminal_response["Result"]["DT"])?is_array($this->Terminal_response["Result"]["DT"])?"":"'".$this->Terminal_response["Result"]["DT"]."'":"",
                "TxnRef"        =>  isset($this->getXmlResponse()["TxnRef"])?is_array($this->getXmlResponse()["TxnRef"])?"":$this->getXmlResponse()["TxnRef"]:"",
                "CardPAN"       =>  isset($this->Terminal_response["Result"]["CN"])?is_array($this->Terminal_response["Result"]["CN"])?"":"'".$this->Terminal_response["Result"]["CN"]."'":"",
                "Receipt"       =>  "",
                "CardType"      =>  isset($this->Terminal_response["Result"]["CT"])?is_array($this->Terminal_response["Result"]["CT"])?"":"'".$this->Terminal_response["Result"]["CT"]."'":"",
                "AuthCode"      =>  isset($this->Terminal_response["Result"]["AC"])?is_array($this->Terminal_response["Result"]["AC"])?"":"'".$this->Terminal_response["Result"]["AC"]."'":"",
                "status"        =>  $status=="00"?"'SUCCESS'":"'FAILED'",//STATIC RESPONSE 
                "ID"            =>  "",
                "result"        =>  $status=="00"?true:false,
                "surcharge"     =>  0,
                "response"      =>  $this->getXmlResponse()
            ];
        
            Log::info("Response Windcave :");
            Log::info(json_encode($this->output));
            Log::info("End Transaction ...");
            Log::info("######################################################################################\n");
        
        
        return true;
    }

    public function query()
    {
        
        $ch = curl_init($this->request_uri);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        $this->api_response = curl_exec($ch);

        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        //echo var_dump($this->api_response);die;
        
        curl_close($ch);
        
    }

    public function setPayload(){
        
        $this->unique_txn=$this->request_data["txn_identifier"];
        $tableNo = round($this->request_data["table_no"]);
        $amount = $this->request_data["total_amount"];
        $currency_code = $this->request_data["currency_code"];
        
        Log::info("Requested Kiosk : Type Status ");
        Log::info(json_encode($this->request_data));
        
        
        $XMLPayload=null;
        $payload=[
            "Scr" => [
              "TxnType"       =>  "Status",
              "TxnRef"        =>  $this->unique_txn,
              "Station"       =>  $this->station_id
            ]
        ];
        
        $XMLPayload = ArrayToXml::convert($payload);
        $XMLPayload = str_replace("<Scr>", "<Scr action=\"doScrHIT\" user=\"".$this->partner_id."\" key=\"".$this->partner_secret."\">",$XMLPayload);
        
        $this->payload =$XMLPayload;
    }
    public function getPayload(){
        return $this->payload;
    }
    public function getXmlResponse(){
        $xml = simplexml_load_string($this->api_response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }
    public function doRequest(){
        
        $this->json_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/xml";

        $this->api_endpoint = $this->api_url;

        $this->request_uri = $this->api_endpoint;

        $this->query();
    }
    
}