<?php
	
	namespace App\lib\PaymentGatewayManager\Drivers\GoPay; 

    use App\lib\PaymentGatewayManager\Drivers\GoPay\Contract\BaseGoPay;

    use App\Events\GoPayLog;

    use App\lib\PaymentGatewayManager\Util\PaymentHelper;

	class Report extends BaseGoPay
	{
		public function process($data)
		{
			$this->request_data = $data;

			$this->getConfig();

			$logs = $this->transaction_model->getLogByType($this->outlet_id, 1, $this->request_data['payload']['start_date'], $this->request_data['payload']['end_date']);

			$csv_data = [];

			$filename = 'grabpay_report.csv';

			foreach($logs as $log)
			{
				$request = json_decode($log->request);
				$orig_tx_id = $request->partnerTxID;
				$query_data = $this->queryPaymentStatus($orig_tx_id, $log->currency_code);

				$input = [
					$log->txn_date,
					$log->order_id,
					$log->table_no,
					$log->total_amount,
					$query_data['status'],
					$query_data['response_code']
				];

				array_push($csv_data, $input);
			}

			$result = PaymentHelper::saveReportToCsv($csv_data, $filename);

			$this->response_code = 200;
			$this->http_code = 200;

			if(!$result)
			{
				$this->response_code = 500;
				$this->http_code = 500;
				return false;
			}

			$this->output = $filename;

			return true;
		} 

		public function queryPaymentStatus($orig_tx_id, $currency_code)
	    {
            $this->getConfig();

            $this->getTxnId();
            
            $this->getMsgId();

            $this->getTransactionDate();

            $this->api_payload = [
                "msgID" => $this->msg_id, 
                "grabID" => $this->grab_id, 
                "terminalID" => $this->terminal_id, 
                "currency" => $currency_code,
                "txType" => "P2M",
                "partnerTxID" => $this->txn_id
            ];

            $this->json_payload = null;

            $query_params = http_build_query($this->api_payload);

            $this->http_method = "GET";  

            $this->application_type = "application/x-www-form-urlencoded";

            $this->api_endpoint = "/grabpay/partner/v1/terminal/transaction/" . $orig_tx_id . "?" . $query_params;

            $this->request_uri = $this->api_url . $this->api_endpoint;

            $this->getSignature();

            $this->query();

            $output = [
                'status' => 'Approved',
				'response_code' => $this->http_code
            ];

            if($this->handleResult() == false)
            {
                $output = [
					'status' => ucfirst($this->api_response->reason),
					'response_code' => $this->http_code
				];
            }

            return $output;
	    }

        public function query()
        {
            $ch = curl_init($this->request_uri);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: ' . $this->partner_id . ":" . $this->signature,
                'Content-Type: ' . $this->application_type,
                'Date: ' . $this->order_date
            ]);

            $this->api_response = json_decode(curl_exec($ch));

            $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $info = curl_getinfo($ch);

            curl_close($ch);
        }

        public function handleResult()
        {
            $error = $this->checkCommonErrors();

            if($error)
            {
                return false;
            }

            return true;
        }
	}