<?php

    namespace App\lib\PaymentGatewayManager\Drivers\GoPay; 

    use App\lib\PaymentGatewayManager\Drivers\GoPay\Contract\BaseGoPay;

    use App\Events\GoPayLog;

    class Reversal extends BaseGoPay
    {
        private $orig_tx_id;

        public function process($data)
        {
            $this->op_type = "ORDER_REVERSAL";

            $this->request_data = $data;

            $this->getConfig();

            $this->orig_tx_id = $this->request_data["payload"]["txn_identifier"];

            $order_data = $this->transaction_model->getTransactionByTxnId($this->request_data["payload"]["txn_identifier"], 1, 1, $this->outlet_id);

            if(!$order_data)
            {
                $this->error_messages = ["messages" => ['Transaction not found']];
                return $this->setDataNotFoundError();
            }

            $this->getMsgId();
                  
            $this->getTxnId();
            
            $this->getTransactionDate();
            
            $this->orig_tx_id = $this->request_data["payload"]["txn_identifier"];
            
            $this->api_payload = [
                "msgID" => $this->msg_id, 
                "grabID" => $this->grab_id, 
                "terminalID" => $this->terminal_id, 
                "currency" => $this->request_data["payload"]["currency_code"],
                "partnerTxID" => $this->txn_id,
                "origPartnerTxID" => $this->orig_tx_id
            ];

            $this->json_payload = json_encode($this->api_payload, JSON_UNESCAPED_SLASHES);

            $this->http_method = "POST";  

            $this->application_type = "application/json";

            $this->api_endpoint = '/v2/' . $this->orig_tx_id . "/cancel";

            $this->request_uri = $this->api_url . $this->api_endpoint;

            //$this->getSignature();

            $this->query();

            if($this->handleResult() == false)
            {
                return false;
            }   

            $this->api_response = $this->api_payload;
            
            $this->output = [
                "code" => $this->http_code,
                "status" => 'SUCCESS',
                "txn_identifier" => $this->orig_tx_id,
            ];
            
            $this->message_type = 'SUCCESS_REVERSAL';

            event(new GoPayLog($this->logData('INFO',1)));

            /*
            $this->updatePaymentStatus([
                "status" => 4,
                "order_id" => $this->request_data['payload']['order_id'],
                "transaction_id" => $this->orig_tx_id,
                "created_by" => $this->outlet_id,
            ]);
            */
            
            return true;
        }

        public function query()
        {
            $ch = curl_init($this->request_uri);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: ' . "Basic U0ItTWlkLXNlcnZlci1ueXBzTlc1d2dGeWluQ3k5TUotcHIySVo6",
                'Content-Type: ' . $this->application_type
            ]);

            $this->api_response = json_decode(curl_exec($ch));

            $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);
        }

        public function handleResult()
        {
            $this->response_code = $this->http_code;

            if($this->http_code != 200)
            {
                switch($this->http_code)
                {
                    case 401:
                        $this->message_type = "AUTH_ERROR";
                        $this->error_messages = ["messages" => [$this->api_response->message]];
                        break;

                    case 404:
                        $this->message_type = "OUTPUT_ERROR";
                        $this->error_messages = ["messages" => [isset($this->api_response->reason) ? $this->api_response->reason : $this->api_response->message]];
                        break;

                    default:
                        $this->message_type = "INPUT_ERROR";
                        $this->error_messages = ["messages" => [isset($this->api_response->reason) ? $this->api_response->reason : $this->api_response->arg]];
                        break;
                }

                event(new GoPayLog($this->logData('ERROR',0)));

                return false;
            }

            return true;
        }
    }