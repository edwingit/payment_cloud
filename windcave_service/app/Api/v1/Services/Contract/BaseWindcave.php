<?php

    namespace App\Api\v1\Services\Contract; 

    use App\Library\Bases\BasePayment;

    use App\Api\v1\Models\Config;
    use App\Api\v1\Models\Log;
    use App\Api\v1\Models\Transaction;
    use App\Api\v1\Services\Listeners\LogEventListener;
    use App\Api\v1\Services\Events\LogEvent;
    
    use App\Library\Constants\ApiMessage;
    use App\Library\Constants\ApiCode;

    use App\Library\Helpers\PaymentHelper;

    abstract class BaseWindcave extends BasePayment
    {
        protected $app_id;
        protected $merchat_id;
        protected $partner_id;
        protected $partner_secret;
        protected $grab_id;
        protected $terminal_id;
        protected $api_url;
        protected $request_uri;
        protected $signature;
        protected $json_payload;

        protected $order_date;
        protected $application_type;
        protected $api_endpoint;
        protected $msg_id;
        protected $txn_id;

        protected $log_model;
        protected $config_model;
        protected $transaction_model;

        protected $transaction_date;
        
        public function __construct()
        {
            $this->log_model = new Log();
            $this->config_model = new Config();
            $this->transaction_model = new Transaction();
        }

        public function getConfig()
        {
            $this->merchant_id = $this->request_data["merchant_id"];
            
            $config_data = $this->config_model->getByTerminal($this->merchant_id,$this->request_data["table_no"]);
            
            $this->partner_id = $config_data[0]->partner_id;
            $this->partner_secret = $config_data[0]->partner_secret;
            
            $this->station_id = $config_data[0]->station_id;
            
            $this->device_id = $config_data[0]->device_id;
            $this->pos_name = $config_data[0]->pos_name;
            $this->pos_version = $config_data[0]->pos_version;
            $this->vendor_id = $config_data[0]->vendor_id;
            
            $this->currency = $config_data[0]->currency;
            $this->terminal_id = $config_data[0]->terminal_id;
            $this->api_url = $config_data[0]->api_url;
            $this->txn_id = $this->request_data["txn_identifier"];
        }

        protected function getSignature()
        {
            $content_digest = "";

            if($this->json_payload)
            {
                $hash = hash("sha256", $this->json_payload);
                $content_digest = base64_encode(hex2bin($hash));
            }

            $this->order_date = gmdate('D, d M Y H:i:s T');

            $header_string = implode("\n", [$this->http_method, $this->application_type, $this->order_date, $this->api_endpoint, $content_digest]);
            $string_to_sign = implode("", [$header_string, "\n"]);

            $hmac = hash_hmac('sha256', $string_to_sign, $this->partner_secret);
            $this->signature = base64_encode(hex2bin($hmac));
        }

        protected function checkCommonErrors()
        {
            $this->response_code = $this->http_code;
            
            if($this->http_code != 200)
            {
                
                switch($this->http_code)
                {
                    case 401:
                        $this->message_type = "AUTH_ERROR";
                        $this->error_messages = ["messages" => [$this->api_response]];
                        break;

                    case 404:
                        $this->message_type = "OUTPUT_ERROR";
                        $this->error_messages = ["messages" => [isset($this->api_response->reason) ? $this->api_response->reason : $this->api_response->message]];
                        break;

                    default:
                        
                        $this->message_type = "INPUT_ERROR";
                        $this->error_messages = ["messages" => [isset($this->api_response->reason) ? $this->api_response->reason : "No Connection"]];
                        break;
                }

                return true;
            }

            return false;
        }

        protected function logData($message_type, $status)
        {
            return $log_data = [
                "log_level"         => $message_type, 
                "op_type"           => $this->op_type, 
                "request_data"      => $this->request_data, 
                "message_type"      => $this->message_type, 
                "http_code"         => $this->http_code, 
                "api_request"       => $this->api_payload, 
                "api_response"      => $this->api_response,
                "msg_id"            => $this->msg_id,
                "txn_id"            => $this->txn_id,
                "transaction_date"  => $this->transaction_date,
                "status"            => $status,
                "endpoint"          => $this->request_uri,
                "outlet_id"         => $this->outlet_id
            ];
        }

        protected function getTransactionDate()
        {
            date_default_timezone_set(self::TIMEZONE);
            $this->transaction_date = date('Y-m-d H:i:s', time());
        }

        protected function getMsgId() 
        {
            $this->msg_id = bin2hex(random_bytes(16));
            
            $msg_is_exist = $this->log_model->getMsgId($this->msg_id);
            
            if($msg_is_exist)
            {
                $this->getMsgId();
            }
        }

        protected function getTxnId() 
        {
            $this->txn_id = bin2hex(random_bytes(16));

            $txn_is_exist = $this->log_model->getTxnId($this->txn_id);
            
            if($txn_is_exist)
            {
                $this->getTxnId();
            }
        }

        abstract public function query();
        //abstract public function handleResult();
        abstract public function process($data);
        
        public function getResponseTerminal($str){
            $response = NULL;

            switch($str){
                case    "P4":
                    $response="PosDeviceId is greater than 32 characters";
                    break;
                case    "P5":
                    $response="PosDeviceId not matched / Station id is wrong.";
                    break;
                case    "P7":
                    $response="Invalid transaction type";
                    break;
                case    "P8":
                    $response="Authentication error";
                    break;
                case    "P9":
                    $response="Authentication error—Station Id mismatch";
                    break;
                case    "PA":
                    $response="Status request error";
                    break;
                case    "PB":
                    $response="SCRHIT Init Session Error";
                    break;
                case    "PC":
                    $response="Existing Txn In progress—previous transaction was left in an incomplete state.\nPlease send Last Transaction and it's reference.";
                    break;
                case    "PD":
                    $response="SCRHIT Transmit Error— network connection issue, ensure the terminal has performed a Logon to the Windcave HOST";
                    break;
                case    "PE":
                    $response="SCRHIT Transmit Error— network connection issue, ensure the terminal has performed a Logon to the Windcave HOST";
                    break;
                case    "PF":
                    $response="SCRHIT Transmit Error— network connection issue, ensure the terminal has performed a Logon to the Windcave HOST";
                    break;
                case    "PG":
                    $response="Init Wait Timeout";
                    break;
                case    "PJ":
                    $response="TxnRef not matched";
                    break;
                case    "PK":
                    $response="SCRHIT not enabled";
                    break;
                case    "PL":
                    $response="Invalid input parameter";
                    break;
                case    "PM":
                    $response="Txn type not allowed";
                    break;
                case    "PO":
                    $response="Invalid Station Id";
                    break;
                case    "TQ":
                    $response="HIT Start Failed— connection lost, ensure the terminal has performed a Logon to the Windcave HOST.";
                    break;
                default:
                    $response="Undefined Response";
                    break;
            }

            return $response;

        }
        
        public function handleResult()
        {

            $error = $this->checkCommonErrors();
            
            if($error)
            {
                
                event(new LogEvent($this->logData('ERROR', 0)));
                return false;   
            }

            return true;
        }
    }