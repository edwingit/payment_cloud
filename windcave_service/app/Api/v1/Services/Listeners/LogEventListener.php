<?php

    namespace App\Api\v1\Services\Listeners; 

    use App\Library\Bases\BaseLogListener;

    use App\Library\Bases\LogEventInterface;

    use App\Api\v1\Models\Log;
    use App\Api\v1\Models\Transaction;
    
    class LogEventListener extends BaseLogListener
    {
        private $log_data;
        private $log_date;
        private $app_id;
        private $merchant_id;
        private $customer_id;
        private $transaction_date;
        private $order_id;
        private $order_no;
        private $table_no;
        private $total_amount;
        private $currency_code;
        private $log_level;
        private $message_type;
        private $api_payload;
        private $api_response;
        private $op_type;
        private $http_code;
        private $msg_id;
        private $txn_id;
        private $status;
        private $endpoint;
        private $outlet_id;
        private $client_info;

        public function handle(LogEventInterface $log)
        {
            
            $this->app_id = $log->request_data["app_id"];
            $this->merchant_id = $log->request_data["merchant_id"];
            $this->customer_id = $log->request_data["customer_id"];
            $this->order_id = $log->request_data["order_id"];
            $this->table_no = $log->request_data["table_no"];
            $this->total_amount = $log->request_data["total_amount"];
            $this->currency_code = $log->request_data["currency_code"];
            $this->client_info = json_decode($log->request_data["client_info"]);

            $this->transaction_date = $log->transaction_date;
            $this->log_level = $log->log_level;
            $this->op_type = $log->op_type;
            $this->message_type = $log->message_type;
            $this->api_payload = $log->api_payload;
            $this->api_response = $log->api_response;
            $this->http_code = $log->http_code;
            $this->msg_id = $log->msg_id;
            $this->txn_id = $log->request_data["txn_identifier"];
            $this->status = $log->status;
            
            $this->logToDB();
        }

        private function logToDB()
        {
            switch($this->op_type)
            {
                case "ORDER_CREATE":
                    $op_type_code = 1;
                    break;
                case "PURCHASE_CREATE":
                    $op_type_code = 1;
                    break;
                case "ORDER_REVERSAL":
                    $op_type_code = 2;
                    break;

                case "ORDER_REFUND":
                    $op_type_code = 3;
                    break;

                case "ORDER_QUERY":
                    $op_type_code = 4;
                    break;
                case "GET_STATUS":
                    $op_type_code = 5;
                    break;
                case "GET_RECEIPT":
                    $op_type_code = 6;
                    break;
                default:
                    $op_type_code = 7;
                    break;
            }

            $input = [
                "order_id" => $this->order_id,
                "customer_id" => $this->customer_id,
                "total_amount" => $this->total_amount,
                "table_no" => $this->table_no,
                "currency_code" => $this->currency_code,
                "msg_id" => $this->msg_id,
                "txn_id" => $this->txn_id,
                "status" => $this->status,
                "merchant_id" => $this->merchant_id,
                "txn_date" => $this->transaction_date,
                "created_date" => $this->transaction_date,
                "updated_date" => $this->transaction_date,
                "op_type" => $op_type_code,
                "app_id" => $this->app_id,
                "client_info" => json_encode($this->client_info, JSON_PRETTY_PRINT),
                "request" => json_encode($this->api_payload, JSON_PRETTY_PRINT),
                "response" => json_encode($this->api_response, JSON_PRETTY_PRINT),
            ];

            $this->logOperation($input);

            if($op_type_code == 1 || $op_type_code == 2)
            {
                $this->logTransaction($input);
            }
        }

        private function logOperation($input)
        {
            $model = new Log();
            $model->saveData($input);
        }

        private function logTransaction($input)
        {
            $model = new Transaction();

            $transaction_data = $model->getTransactionByTxnId($input["txn_id"],$input["merchant_id"]);

            if($transaction_data->isEmpty())
            {
                $model->saveData($input);
            }
            
        }
        
    }