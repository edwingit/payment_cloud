<?php

    namespace App\Api\v1\Providers;

    use App\Api\v1\Routes\ApiRoutes;

    use Illuminate\Support\ServiceProvider;

    class ApiRouteServiceProvider extends ServiceProvider
    {
        public function register()
        {
            $registry = $this->app->make(ApiRoutes::class);

            if (!is_object($registry)) 
            {
                Log::info('Not adding any service routes - route file is missing');
                return;
            }
          
            $registry->bind(app());
        }
    }
