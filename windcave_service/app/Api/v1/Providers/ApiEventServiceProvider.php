<?php

    namespace App\Api\v1\Providers;

    use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

    class ApiEventServiceProvider extends ServiceProvider
    {
        /**
         * The event listener mappings for the application.
         *
         * @var array
         */
        protected $listen = [
            'App\Api\v1\Events\AuthLogEvent' => [
                'App\Api\v1\Listeners\AuthLogEventListener',
            ],
        ];
    }
