<?php

    namespace App\Api\v1\Requests;

    use App\Library\Bases\BaseRepository;

    class Repository extends BaseRepository 
    {
        private $merchant_id;
        private $customer_id;
        private $mode;
        private $order_id;
        private $table_no;
        private $total_amount;
        private $currency_code;
        private $txn_identifier;
        private $status;
        private $start_date;
        private $end_date;
        private $client_info;
        private $auth_header;
        private $result;
       
        public function setOperation($operation_type) 
        {
            $this->operation_type = $operation_type;
        }

        public function getInput() 
        {
            $this->auth_header = $this->request->header('X-AUTHORIZATION');
            $this->merchant_id = isset($this->json_data["merchant_id"]) && $this->json_data["merchant_id"] ? $this->json_data["merchant_id"] : "";
            $this->customer_id = isset($this->json_data["customer_id"]) && $this->json_data["customer_id"] ? $this->json_data["customer_id"] : "";
            $this->mode = isset($this->json_data["mode"]) && $this->json_data["mode"] ? $this->json_data["mode"] : "";
            $this->order_id = isset($this->json_data["order_id"]) ? $this->json_data["order_id"] : "";
            $this->table_no = isset($this->json_data["table_no"]) ? $this->json_data["table_no"] : "";
            $this->total_amount = isset($this->json_data["total_amount"]) ? $this->json_data["total_amount"] : "";
            $this->currency_code = isset($this->json_data["currency_code"]) ? $this->json_data["currency_code"] : "";
            $this->txn_identifier = isset($this->json_data["txn_identifier"]) ? $this->json_data["txn_identifier"] : "";
            $this->order_no = isset($this->json_data["orderNo"]) ? $this->json_data["orderNo"] : "";
            $this->status = isset($this->json_data["status"]) ? $this->json_data["status"] : "";
            $this->start_date = isset($this->json_data["start_date"]) ? $this->json_data["start_date"] : "";
            $this->end_date = isset($this->json_data["end_date"]) ? $this->json_data["end_date"] : "";
            $this->client_info = isset($this->json_data["client_info"]) ? $this->json_data["client_info"] : "";
        }

        public function setValidationData() 
        {
            //dd($this->client_info);die;
            $this->data = [
                "auth_header"       => $this->auth_header,
                "merchant_id"       => $this->merchant_id,
                "customer_id"       => $this->customer_id,
                "mode"              => $this->mode,
                "order_id"          => $this->order_id,
                "table_no"          => $this->table_no,
                "total_amount"      => $this->total_amount,
                "currency_code"     => $this->currency_code,
                "txn_identifier"    => $this->txn_identifier,
                "status"            => $this->status,
                "start_date"        => $this->start_date,
                "end_date"          => $this->end_date,
                "client_info"       => json_encode($this->client_info),
                "op_type"           => $this->operation_type,
            ];
        }

        public function setValidationRules() 
        {
            
            switch($this->operation_type)
            {
                case "order":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_id" => "required|integer",
                        "customer_id" => "required|integer",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "order_id" => "required|integer",
                        "table_no" => "required|string",
                        "total_amount" => "required|numeric",
                        "currency_code" => "required|string",
                        "client_info" => "required|json",
                    ];

                    break;
                case "purchase":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_id" => "required|integer",
                        "customer_id" => "required|integer",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "order_id" => "required|integer",
                        "table_no" => "required|string",
                        "total_amount" => "required|numeric",
                        "currency_code" => "required|string",
                        "client_info" => "required|json",
                        "txn_identifier" => "required|string"
                    ];

                    break;

                case "query":
                case "reversal":
                case "refund":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_id" => "required|integer",
                        "customer_id" => "required|integer",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "order_id" => "required|integer",
                        "table_no" => "required|string",
                        "total_amount" => "required|numeric",
                        "txn_identifier" => "required|string",
                        "currency_code" => "required|string",
                        "client_info" => "required|json",
                    ];

                    break;

                case "cancel":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_key" => "required",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "order_id" => "required|string",
                        "table_no" => "required|string",
                        "txn_identifier" => "required|string",
                        "currency_code" => "required|string",
                    ];

                    break;

                case "status":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_id" => "required|integer",
                        "customer_id" => "required|integer",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "order_id" => "required|integer",
                        "table_no" => "required|string",
                        "total_amount" => "required|numeric",
                        "currency_code" => "required|string",
                        "client_info" => "required|json",
                    ];

                    break;
                case "receipt":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_id" => "required|integer",
                        "customer_id" => "required|integer",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "order_id" => "required|integer",
                        "table_no" => "required|string",
                        "total_amount" => "required|numeric",
                        "currency_code" => "required|string",
                        "client_info" => "required|json",
                    ];

                    break;

                case "get_notification":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_key" => "required",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "msgID" => "required",
                        "grabID" => "required",
                        "terminalID" => "required|string",
                        "currency" => "required",
                        "txType" => "required",
                        "partnerTxID" => "required"
                    ];

                    break;

                case "get_logs":
                    $this->rules = [
                        "auth_header" => "required",
                        "merchant_key" => "required",
                        "mode" => "required|in:smarttab-ios,smarttab-android,kiosk-ios,kiosk-android",
                        "start_date" => "required",
                        "end_date" => "required",
                    ];
                    
                    break;

                case "get_report":
                    $this->rules = [];
                    break;
                default:
                    $this->rules = [];
                    break;
            }
        }

    }
