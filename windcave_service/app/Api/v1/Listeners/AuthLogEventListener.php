<?php

    namespace App\Api\v1\Listeners; 

    use App\User;

    use App\Events\ApiLog;

    use App\Library\Bases\BaseLogListener;

    use App\Library\Bases\LogEventInterface;

    use App\Api\v1\Models\AuthLog;
    
    class AuthLogEventListener extends BaseLogListener
    {
        private $origin_ip;
        private $origin_merchant_id;
        private $app_id;
        private $auth_header;
        private $message_type;
        private $response_code;
        private $log_input;
        
        public function handle(LogEventInterface $log)
        {
            $log_time = date("Y-m-d H:i:s", time());

            $this->log_input = [
                "origin_ip" => $log->origin_ip,
                "origin_merchant_id" => $log->origin_merchant_id,
                "app_id" => $log->app_id,
                "auth_header" => $log->auth_header,
                "message_type" => $log->message_type,
                "response_code" => $log->response_code,
                "created_at" => $log_time,
                "updated_at" => $log_time,
            ];

            $model = new AuthLog();
            $model->saveData($this->log_input);
        }
    }