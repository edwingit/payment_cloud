<?php

    namespace App\Api\v1\Services\Events;

    use Illuminate\Queue\SerializesModels;

    use App\Library\Bases\LogEventInterface;

    class LogEvent implements LogEventInterface
    {
        use SerializesModels;
        
        public $log_level;
        public $op_type;
        public $request_data;
        public $message_type;
        public $http_code;
        public $api_payload;
        public $api_response;
        public $msg_id;
        public $txn_id;
        public $transaction_date;
        public $status;
        
        public function __construct($input)
        {
            $this->log_level = $input["log_level"];
            $this->op_type = $input["op_type"];
            $this->request_data = $input["request_data"];
            $this->message_type = $input["message_type"];
            $this->http_code = $input["http_code"];
            $this->api_payload = $input["api_request"];
            $this->api_response = $input["api_response"];
            $this->transaction_date = $input["transaction_date"];
            $this->status = $input["status"];
        }
    }