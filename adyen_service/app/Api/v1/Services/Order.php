<?php
  
namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Services\Events\LogEvent;
use Spatie\ArrayToXml\ArrayToXml;

class Order extends BaseWindcave
{
    private $payload; 
    private $unique_txn;
    private $Terminal_response;
    private $receipt;
    
    
    public function process($data)
    {
        
        $this->op_type = "ORDER_CREATE";

        $this->request_data = $data;

        $this->getConfig();
      
        //$this->getMsgId();

        //$this->getTxnId();
        
        $status = "UNDEFINED";
        
        //convert array to xml
        //step 1
        $this->setPayload("purchase");
        $this->getPayload();
        $this->doRequest();
        
        //Request give response before do trx
        if(isset($this->getXmlResponse()["Complete"]) && $this->getXmlResponse()["Complete"]==1){
            if(isset($this->getXmlResponse()["Result"])){
                $this->Terminal_response = $this->getXmlResponse()["Result"];
                $status = $this->getResponseTerminal($this->getXmlResponse()["Result"]["RC"]);
            }
        }
        
        if($this->handleResult() == false)
        {
          return false;
        }
        
        //sleep(1);
        
        $n=0;
        while(true){
            //step 2
            $this->setPayload("status");
            $this->getPayload();
            $this->doRequest();
            
            $n++;
            sleep(1);
            //echo "\nProcess ".$n." sec. at ".date("Y-m-d H:i:s")."\n";
            
            if(isset($this->getXmlResponse()["DL1"])){ //trx status APPROVED/DECLINED
                //echo ">>>>".$this->getXmlResponse()["DL1"];
                //echo var_dump($this->getXmlResponse());
                
                $status = $this->getXmlResponse()["DL1"];
                if($status=="TRANSACTION"){
                    $status = "CANCELLED"; //USER PRESS CANCEL BUTTON BEFORE/AFTER SWIPE
                }
            }
            
            if(isset($this->getXmlResponse()["Complete"]) && $this->getXmlResponse()["Complete"]==1){
                if(isset($this->getXmlResponse()["StatusId"]) && $this->getXmlResponse()["StatusId"]==6){
                    
                            
                    if(isset($this->getXmlResponse()["Result"])){
                        $this->Terminal_response = $this->getXmlResponse()["Result"];
                        
                        $ap= is_array($this->getXmlResponse()["Result"]["AC"])?"":$this->getXmlResponse()["Result"]["AC"];
                        $cn= is_array($this->getXmlResponse()["Result"]["CN"])?"":$this->getXmlResponse()["Result"]["CN"];
                        $ct= is_array($this->getXmlResponse()["Result"]["CT"])?"":$this->getXmlResponse()["Result"]["CT"];
                        $ch= is_array($this->getXmlResponse()["Result"]["CH"])?"":$this->getXmlResponse()["Result"]["CH"];
                        
                        /*
                        echo "\nApproval Code : ".$ap;
                        echo "\nCard Number   : ".$cn;
                        echo "\nCard Type     : ".$ct;
                        echo "\nCard Holder   : ".$ch;
                        echo "\nTransaction   : ".$status;
                        echo "\n\n";
                         * */
                         
                    }
                    
                    if(isset($this->getXmlResponse()["Rcpt"])){
                        $this->receipt=$this->getXmlResponse()["Rcpt"];
                    }
                    
                }
                
                break;
            }
            
            //for signature ?
            if(isset($this->getXmlResponse()["TxnStatusId"]) && $this->getXmlResponse()["TxnStatusId"]==7){
                $this->Terminal_response = $this->getXmlResponse();
                break;
            }
                 
            //set timeout in 1 mins
            if($n==60){
                //echo "\nTransaction was Timeout.";
                break;
            }
            
        }
        
        $this->message_type = "Transaction : ".$status;
        $this->output = [
                "status"    =>  $status=="APPROVED"?true:false,//STATIC RESPONSE 
                "response"  =>  $this->Terminal_response
            ];
        
        return true;
    }

    public function query()
    {
        
        $ch = curl_init($this->request_uri);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        $this->api_response = curl_exec($ch);

        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
    }

    public function handleResult()
    {
        
        $error = $this->checkCommonErrors();
        
        if($error)
        {
            event(new LogEvent($this->logData('ERROR', 0)));
            return false;
        }

        return true;
    }
    
    public function setPayload($type){
        $this->unique_txn=$this->request_data["txn_identifier"];
        $tableNo = round($this->request_data["table_no"]);
        $amount = $this->request_data["total_amount"];
        $currency_code = $this->request_data["currency_code"];
        
        $XMLPayload=null;
        $payload=[];
        
        switch($type){
            case "purchase":
                $payload=[
                    "Scr" => [
                      "Amount"        =>  $amount,
                      "Cur"           =>  $currency_code,
                      "TxnType"       =>  "Purchase",
                      "TxnRef"        =>  $this->unique_txn,
                      "DeviceId"      =>  $this->device_id,
                      "PosName"       =>  $this->pos_name,
                      "PosVersion"    =>  $this->pos_version,
                      "VendorId"      =>  $this->vendor_id,
                      "MRef"          =>  $this->unique_txn,
                      "Station"       =>  $this->station_id
                    ]
                ];
                
                break;
            case "status":
                $payload=[
                    "Scr" => [
                      "TxnType"       =>  "Status",
                      "TxnRef"        =>  $this->unique_txn,
                      "Station"       =>  $this->station_id
                    ]
                ];
                
                break;
            default :
                break;
        }
        
        $XMLPayload = ArrayToXml::convert($payload);
        $XMLPayload = str_replace("<Scr>", "<Scr action=\"doScrHIT\" user=\"".$this->partner_id."\" key=\"".$this->partner_secret."\">",$XMLPayload);
        
        
        $this->payload =$XMLPayload;
    }
    public function getPayload(){
        return $this->payload;
    }
    public function getXmlResponse(){
        $xml = simplexml_load_string($this->api_response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }
    
    public function doRequest(){
        
        $this->json_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/xml";

        $this->api_endpoint = $this->api_url;

        $this->request_uri = $this->api_endpoint;
        
        $this->query();
    }
    
    public function getResponseTerminal($str){
        $response = NULL;
        
        switch($str){
            case    "P4":
                $response="PosDeviceId is greater than 32 characters";
                break;
            case    "P5":
                $response="PosDeviceId not matched / Station id is wrong.";
                break;
            case    "P7":
                $response="Invalid transaction type";
                break;
            case    "P8":
                $response="Authentication error";
                break;
            case    "P9":
                $response="Authentication error—Station Id mismatch";
                break;
            case    "PA":
                $response="Status request error";
                break;
            case    "PB":
                $response="SCRHIT Init Session Error";
                break;
            case    "PC":
                $response="Existing Txn In progress—previous transaction was left in an incomplete state.\nPlease send Last Transaction and it's reference.";
                break;
            case    "PD":
                $response="SCRHIT Transmit Error— network connection issue, ensure the terminal has performed a Logon to the Windcave HOST";
                break;
            case    "PE":
                $response="SCRHIT Transmit Error— network connection issue, ensure the terminal has performed a Logon to the Windcave HOST";
                break;
            case    "PF":
                $response="SCRHIT Transmit Error— network connection issue, ensure the terminal has performed a Logon to the Windcave HOST";
                break;
            case    "PG":
                $response="Init Wait Timeout";
                break;
            case    "PJ":
                $response="TxnRef not matched";
                break;
            case    "PK":
                $response="SCRHIT not enabled";
                break;
            case    "PL":
                $response="Invalid input parameter";
                break;
            case    "PM":
                $response="Txn type not allowed";
                break;
            case    "PO":
                $response="Invalid Station Id";
                break;
            case    "TQ":
                $response="HIT Start Failed— connection lost, ensure the terminal has performed a Logon to the Windcave HOST.";
                break;
            default:
                $response="Undefined Response";
                break;
        }
        
        return $response;
        
    }
}