<?php

    namespace App\lib\PaymentGatewayManager\Drivers\GoPay\Listeners; 

    use App\User;

    use App\Events\GoPayLog;

    use App\lib\PaymentGatewayManager\Contract\LogEventInterface;
    
    use App\lib\PaymentGatewayManager\Contract\BaseLogListener;

    use App\lib\PaymentGatewayManager\Drivers\GoPay\Logger\GoPayLogger;

    use App\lib\PaymentGatewayManager\Drivers\GoPay\Models\GoPayTransactions;

    class WindcaveLogListener extends BaseLogListener
    {
        private $log_data;
        private $log_date;
        private $merchant_key;
        private $transaction_date;
        private $order_id;
        private $order_no;
        private $table_no;
        private $total_amount;
        private $currency_code;
        private $log_level;
        private $message_type;
        private $api_payload;
        private $api_response;
        private $op_type;
        private $http_code;
        private $msg_id;
        private $txn_id;
        private $status;
        private $endpoint;
        private $outlet_id;

        public function handle(LogEventInterface $log)
        {
            $this->log_date = date('Y-m-d', time());
            $this->merchant_key = $log->request_data["merchant_key"];

            $this->transaction_date = $log->transaction_date;
            
            $this->order_id = isset($log->request_data["payload"]["order_id"]) ? $log->request_data["payload"]["order_id"] : "N/A";
            $this->table_no = isset($log->request_data["payload"]["table_no"]) ? $log->request_data["payload"]["table_no"] : "N/A";
            $this->total_amount = isset($log->request_data["payload"]["total_amount"]) ? $log->request_data["payload"]["total_amount"] : "N/A";
            $this->currency_code = isset($log->request_data["payload"]["currency_code"]) ? $log->request_data["payload"]["currency_code"] : "N/A";
            $this->log_level = $log->log_level;
            $this->op_type = $log->op_type;
            $this->message_type = $log->message_type;
            $this->api_payload = $log->api_payload;
            $this->api_response = $log->api_response;
            $this->http_code = $log->http_code;
            $this->msg_id = $log->msg_id;
            $this->txn_id = $log->txn_id;
            $this->status = $log->status;
            $this->endpoint = $log->endpoint;
            $this->outlet_id = $log->outlet_id;

            $this->logToFile();
            
            /*
            $this->logToFile();

            if($this->message_type == 'SUCCESS_ORDER' || $this->message_type == 'SUCCESS_REFUND' || $this->message_type == 'SUCCESS_REVERSAL')
            {
                $this->logToDB();
            }
            */

            $this->logToDB();
        }

        private function logToFile()
        {
            
            $path = storage_path('logs/payments/grabpay/' . $this->merchant_key . '/' . $this->log_date);

            $log_file = strtolower($this->log_level) . '.log';

            $logger = new GoPayLogger('text');
            
            $logger->setPath($path . '/' . $log_file);

            $logger->setOpType($this->op_type);

            $logger->log('grab_pay', $this->log_level, $this->message_type, [
                "op_type" => $this->op_type,
                "http_code" => $this->http_code,
                "merchant_key" => $this->merchant_key,
                "transaction_date" => $this->transaction_date,
                "order_id" => $this->order_id,
                "order_no" => $this->order_no,
                "table_no" => $this->table_no,
                "total_amount" => $this->total_amount,
                "currency_code" => $this->currency_code,
                "payload" => $this->api_payload, 
                "messages" => $this->api_response,
                "msg_id" => $this->msg_id,
                "txn_id" => $this->txn_id
            ]);
        }

        private function logToDB()
        {
            /*
            $type = 1;
            
            $qr_code = $this->api_response->qrcode;

            if($this->message_type == 'SUCCESS_REFUND')
            {
                $type = 2;
            }

            if($this->message_type == 'SUCCESS_REVERSAL')
            {
                $type = 3;
            }
            */

            $qr_code = '';

            if($this->message_type == 'SUCCESS_ORDER')
            {
                $qr_code = $this->api_response->qrcode;
            }

            switch($this->op_type)
            {
                case 'ORDER_CREATE':
                    $type = 1;
                    break;

                case 'ORDER_REVERSAL':
                    $type = 2;
                    break;

                case 'ORDER_REFUND':
                    $type = 3;
                    break;

                case 'ORDER_QUERY':
                    $type = 4;
                    break;
            }

            /*
            $user_model = new User();
            $user_data = $user_model->getByCustomerKey($this->merchant_key);
            $user_id = $user_data[0]->id;
            */

            $payment_type = "GRAB PAY";
            $payment_data = $this->getPaymentId($payment_type);
            $payment_id = $payment_data[0]->id;

            $input = [
                "order_id" => $this->order_id,
                "order_no" => "",
                "total_amount" => $this->total_amount,
                "table_no" => $this->table_no,
                "currency_code" => $this->currency_code,
                "msg_id" => $this->msg_id,
                "txn_id" => $this->txn_id,
                "grab_txn_id" => isset($this->api_response->txID) ? $this->api_response->txID : $this->txn_id,
                "qr_code" => $qr_code,
                "status" => $this->status,
                "created_by" => $this->outlet_id,
                "txn_date" => $this->transaction_date,
                "type" => $type,
                "request" => json_encode($this->api_payload, JSON_PRETTY_PRINT),
                "response" => json_encode($this->api_response, JSON_PRETTY_PRINT),
                "endpoint" => $this->endpoint,
                "payment_id" => $payment_id,
                "status" => $this->status
            ];

            $this->logToTransaction($input);
            $this->logToReqPayment($input);
        }

        private function logToTransaction($input)
        {
            $model = new GoPayTransactions();
            $model->saveData($input);
        }
    }