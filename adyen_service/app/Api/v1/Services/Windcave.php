<?php

namespace App\Api\v1\Services; 

use App\Library\Bases\PaymentInterface;

use App\Api\v1\Models\Config;
use App\Api\v1\Models\Merchants;
use App\Api\v1\Models\Transaction;
use Illuminate\Http\Request;

class Windcave implements PaymentInterface
{
    private $processor;
    private $error_type;
    private $error_messages;
    private $response_code;
    private $result;
    private $message_type;
    private $http_code;

    public function makePayment($data)
    {
        $this->processor = new Order();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;

        return $this->processor->output;
    }
    
    public function makePurchase($data)
    {
        $this->processor = new Purchase();

        $this->result = $this->processor->process($data);
        
        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;
        $this->result = $this->processor->result;
        
        return $this->processor->output;
    }

    public function getPayments($data)
    {
        $this->processor = new Query();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;

        return $this->processor->output;
    }	

    public function makePaymentReversal($data)
    {
        $this->processor = new Reversal();

        $this->result = $this->processor->process($data);
        
        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;
        $this->result = $this->processor->result;
        
        return $this->processor->output;
    }

    public function makePaymentRefund($data)
    {
        $this->processor = new Refund();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;

        return $this->processor->output;
    }

    public function updateStatus($data)
    {
        $this->processor = new Status();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;

        return $this->processor->output;
    }

    public function getNotification($data)
    {

    }
   
    public function getReport($data)
    {
        $this->processor = new Report();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;

        return $this->processor->output;
    }

    public function getLogs($data)
    {

    }

    public function checkConfig($user_id,$terminal)
    {
        
        $output = true;

        $model = new Config();
        $config_data = $model->getAll($user_id);
        $config_terminal = $model->getByTerminal($user_id,$terminal);
        
        if($config_terminal->isEmpty())
        {
            $output = false;
        }
        
        return $output;
    }
    
    public function foundReversal($customer_key=null,$order_id=null,$total_amount=null)
    {
        
        $output = true;

        $model = new Merchants();
        $merchant_id=$model->getAccount($customer_key);
        
        if(count($merchant_id) > 0){
            
            $transaction = new Transaction();
            $config_data = $transaction->getTransactionForReversal($merchant_id[0]->id,$order_id,$total_amount);
            
        }else{
            $output=false;
            
        }
        
        return $output;
    }

    private function handleResult()
    {
        $this->response_code = $this->processor->response_code;
          
        if(!$this->result)
        {
            
            $this->message_type = $this->processor->message_type;
            
            $this->error_messages = $this->processor->error_messages;
            
            $this->http_code = $this->processor->http_code;
            
            throw new \Exception($this->processor->message_type, $this->processor->http_code);
        }
    }

    public function getMessageType()
    {
        return $this->message_type;
    }
    
    public function getResult()
    {
        return $this->result;
    }
    
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    public function getResponseCode()
    {
        return $this->response_code;
    }

    public function getHttpCode()
    {
        return $this->http_code;
    }
    public function getAccount($secret_key){

    }
    public function getStatus($data)
    {
        $this->processor = new Status();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;
        $this->result = $this->processor->result;

        return $this->processor->output;
    }
    
    public function getReceipt($data)
    {
        $this->processor = new Receipt();

        $this->result = $this->processor->process($data);

        $this->handleResult();

        $this->response_code = $this->processor->response_code;
        $this->message_type = $this->processor->message_type;
        $this->http_code = $this->processor->http_code;

        return $this->processor->output;
    }
}