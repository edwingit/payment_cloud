<?php
  
namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Services\Events\LogEvent;
use Spatie\ArrayToXml\ArrayToXml;

class Receipt extends BaseWindcave
{
    private $payload; 
    private $unique_txn;
    private $Terminal_response;
    private $receipt;
    
    public function process($data)
    {
        $this->op_type = "GET_RECEIPT";

        $this->request_data = $data;

        $this->getConfig();
        
        $status = "UNDEFINED";
        $message = "";
        //convert array to xml
        //step 1
        $this->setPayload();
        $this->getPayload();
        $this->doRequest();
        
        //Request give response before do trx
        if(isset($this->getXmlResponse()["Complete"]) && $this->getXmlResponse()["Complete"]==1){
            if(isset($this->getXmlResponse()["Result"])){
                
                $status = $this->getResponseTerminal($this->getXmlResponse()["Result"]["RC"]);
                $message = $this->getXmlResponse()["Result"]["RT"];
            }
        }else{
            $status="00";
            $message="APPROVED";
        }
        
        $this->Terminal_response = $this->getXmlResponse();
        
        if($this->handleResult() == false)
        {
          return false;
        }
        
        $this->message_type = "Transaction : ".$message;
        $this->output = [
                "status"    =>  $status=="00"?true:false,//STATIC RESPONSE 
                "response"  =>  $this->Terminal_response
            ];
        
        return true;
    }

    public function query()
    {
        
        $ch = curl_init($this->request_uri);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        $this->api_response = curl_exec($ch);
        
        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
    }

    public function setPayload(){
        
        $this->unique_txn=$this->request_data["txn_identifier"];
        $tableNo = round($this->request_data["table_no"]);
        $amount = $this->request_data["total_amount"];
        $currency_code = $this->request_data["currency_code"];
        
        $XMLPayload=null;
        $payload=[
            "Scr" => [
                "TxnType"       =>  "Receipt",
                "TxnRef"        =>  $this->unique_txn,
                "Station"       =>  $this->station_id,
                "DuplicateFlag" =>  0,
                "ReceiptType"   =>  2
            ]
        ];
        
        $XMLPayload = ArrayToXml::convert($payload);
        $XMLPayload = str_replace("<Scr>", "<Scr action=\"doScrHIT\" user=\"".$this->partner_id."\" key=\"".$this->partner_secret."\">",$XMLPayload);
        
        
        $this->payload =$XMLPayload;
        
    }
    public function getPayload(){
        return $this->payload;
    }
    public function getXmlResponse(){
        $array=null;
        
        try{
            if (strpos($this->api_response, 'TxnRef') !== false) {
                $xml = simplexml_load_string($this->api_response, "SimpleXMLElement", LIBXML_NOCDATA);
                $json = json_encode($xml);
                $array = json_decode($json,TRUE);
            
            }
            
        } catch (Exception $ex) {
            
        }
        
        return $array;
    }
    public function doRequest(){
        
        $this->json_payload = $this->payload;
        $this->api_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/xml";

        $this->api_endpoint = $this->api_url;

        $this->request_uri = $this->api_endpoint;

        $this->query();
    }
    
}