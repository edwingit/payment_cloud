<?php
  
    namespace App\Api\v1\Services; 

  use App\Api\v1\Services\Contract\BaseGoPay;

  use App\Api\v1\Services\Events\LogEvent;

    class Query extends BaseGoPay
    {
        public function process($data)
	    {
            $this->op_type = "ORDER_QUERY";

            $this->request_data = $data;

            $this->getConfig();

            $order_data = $this->log_model->getTransactionByTxnId($this->request_data["txn_identifier"], 1, 1, $this->request_data["merchant_id"]);

            if($order_data->isEmpty())
            {
                $this->error_messages = [ ["txn_id" => 'not found' ] ];
                return $this->setDataNotFoundError();
            }
            
            $this->getTxnId();
            
            $this->getMsgId();

            $this->getTransactionDate();

            $this->orig_tx_id = $this->request_data["txn_identifier"];

            $this->api_payload = [
                "msgID" => $this->msg_id, 
                "grabID" => $this->grab_id, 
                "terminalID" => $this->terminal_id, 
                "currency" => $this->request_data["currency_code"],
                "txType" => "P2M",
                "partnerTxID" => $this->txn_id
            ];

            $this->json_payload = null;

            $query_params = http_build_query($this->api_payload);

            $this->http_method = "GET";  

            $this->application_type = "application/json";

            $this->api_endpoint = '/v2/' . $this->orig_tx_id . "/status";

            $this->request_uri = $this->api_url . $this->api_endpoint;
            
            //$this->getSignature();

            $this->query();

            if($this->handleResult() == false)
            {
                return false;
            }

            $this->output = [
                "code" => $this->response_code,
                "status" => $this->api_response->transaction_status,
                "signature_key" =>  $this->api_response->signature_key,
                "txn_identifier" => $this->orig_tx_id,
            ];

            $this->message_type = 'SUCCESS_QUERY';

            event(new LogEvent($this->logData('INFO',1)));

            return true;
	    }

        public function query()
        {
            $ch = curl_init($this->request_uri);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: ' . "Basic U0ItTWlkLXNlcnZlci1ueXBzTlc1d2dGeWluQ3k5TUotcHIySVo6",
                'Content-Type: ' . $this->application_type
            ]);

            $this->api_response = json_decode(curl_exec($ch));

            $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $info = curl_getinfo($ch);

            curl_close($ch);
        }

        public function handleResult()
        {
            $error = $this->checkCommonErrors();

            if($error)
            {
                event(new LogEvent($this->logData('ERROR', 0)));
                return false;
            }

            return true;
        }
	 }