<?php

    namespace App\lib\PaymentGatewayManager\Drivers\GoPay; 

    use App\lib\PaymentGatewayManager\Drivers\GoPay\Contract\BaseGoPay;

    use App\Events\GoPayLog;

    class Refund extends BaseGoPay
    {
        public function process($data)
        {
            $this->op_type = "ORDER_REFUND";

            $this->request_data = $data;

            $this->getConfig();

            $this->msg_id = bin2hex(random_bytes(16));
            $this->txn_id = $this->request_data["payload"]["txn_identifier"];
            
            $amount = round($this->request_data["payload"]["total_amount"] * 100);

            $order_data = $this->transaction_model->getTransactionByTxnId($this->txn_id, 1);
            $refund_order_data = $this->transaction_model->getTransactionByTxnId($this->request_data["payload"]["txn_identifier"], 2);

            if(!$order_data)
            {
                $this->error_messages = ["messages" => ['Transaction not found']];
                return $this->setDataNotFoundError();
            }

            if($refund_order_data)
            {
                return $this->setTransactionStatus();
            }

            $this->getTransactionDate();

            $this->api_payload = [
                "msgID" => $this->msg_id, 
                "grabID" => $this->grab_id, 
                "terminalID" => $this->terminal_id, 
                "currency" => $this->request_data["payload"]["currency_code"],
                "amount" => $amount,
                "partnerTxID" => bin2hex(random_bytes(16)),
                "origPartnerTxID" => $this->txn_id
            ];

            $this->json_payload = json_encode($this->api_payload, JSON_UNESCAPED_SLASHES);

            $this->http_method = "PUT";  

            $this->application_type = "application/json";

            $this->api_endpoint = '/grabpay/partner/v1/terminal/transaction/' . $this->txn_id . '/refund';

            $this->request_uri = $this->api_url . $this->api_endpoint;

            $this->getSignature();

            $this->query();

            if($this->handleResult() == false)
            {
                return false;
            }

            $this->api_response->qrcode = $order_data[0]->qr_code;
            $this->api_response->txID = $order_data[0]->grab_txn_id;

            $this->output = [
                "msg_id" => $this->api_response->msgID,
                "status" => $this->api_response->status,
                "tx_id" => $this->api_response->txID,
                "origin_tx_id" => $this->api_response->originTxID,
                "status" => $this->api_response->status,
                "description" => $this->api_response->description
            ];

            $this->message_type = 'SUCCESS_REFUND';

            event(new GoPayLog($this->logData('INFO')));

            return true;
        }

        public function query()
        {
              $ch = curl_init($this->request_uri);
              
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLINFO_HEADER_OUT, true);
              
              curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Authorization: ' . $this->partner_id . ":" . $this->signature,
                    'Content-Type: ' . $this->application_type,
                    'Date: ' . $this->order_date
              ]);
              
              $this->api_response = json_decode(curl_exec($ch));

              $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

              curl_close($ch);
        }

        private function setTransactionStatus()
        {
            $this->response_code = 400;
            $this->http_code = 400;
            $this->error_type = "TRANS_ERR";
            $this->message_type = "TRANS_ERR";
            $this->error_messages = ["messages" => ['Transaction already refunded']];
            return false;
        }

        public function handleResult()
        {
            $error = $this->checkCommonErrors();

            if($error)
            {
                event(new GoPayLog($this->logData('ERROR')));
                return false;
            }

            return true;
        }
    }