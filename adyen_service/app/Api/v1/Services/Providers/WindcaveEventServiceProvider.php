<?php

    namespace App\Api\v1\Services\Providers;

    use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

    class WindcaveEventServiceProvider extends ServiceProvider
    {
        /**
         * The event listener mappings for the application.
         *
         * @var array
         */
        protected $listen = [
            'App\Api\v1\Services\Events\LogEvent' => [
                'App\Api\v1\Services\Listeners\LogEventListener',
            ],
        ];
    }
