<?php
  
namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Services\Events\LogEvent;
use Spatie\ArrayToXml\ArrayToXml;

use Log;

class Status extends BaseWindcave
{
    private $payload; 
    private $unique_txn;
    private $Terminal_response;
    private $receipt;
    
    public function process($data)
    {
        $this->op_type = "GET_STATUS";

        $this->request_data = $data;

        $this->getConfig();
        
        $status = "UNDEFINED";
        $message = "";
        //convert array to xml
        //step 1
        $this->setPayload();
        
        $this->getPayload();
        
        Log::info("Requested >>>>>>>>>>>>>>>>");
        Log::info(json_encode($this->getPayload()));
        
        $this->doRequest();
        
        if($this->handleResult() == false)
        {
            return false;
        }
        
        $response = json_decode($this->api_response);
        
        
        if(isset($response->SaleToPOIRequest->EventNotification)){ //NO RESPONSE OR EVENT FROM TERMINAL
            $this->http_code=201;
            $this->response_code=201;
            $find = array("+","message=");
            $this->message_type = str_replace($find," ",$response->SaleToPOIRequest->EventNotification->EventDetails);
            $this->result=false;
            
        }else{//any event from terminal
            
            $this->http_code=201;
            
            
            if(isset($response->SaleToPOIResponse->TransactionStatusResponse->RepeatedMessageResponse)){
                
                $paymentResponse=$response->SaleToPOIResponse->TransactionStatusResponse->RepeatedMessageResponse->RepeatedResponseMessageBody->PaymentResponse;
             
                if($paymentResponse->Response->Result=="Success"){
                    
                    $this->output = [
                        "ID"            =>  isset($paymentResponse->SaleData->SaleTransactionID->TransactionID)?$paymentResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "STAN"          =>  isset($paymentResponse->SaleData->SaleTransactionID->TransactionID)?$paymentResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "TxnRef"        =>  isset($paymentResponse->SaleData->SaleTransactionID->TransactionID)?$paymentResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "MerchantId"    =>  isset($paymentResponse->PaymentResult->PaymentAcquirerData->MerchantID)?$paymentResponse->PaymentResult->PaymentAcquirerData->MerchantID:"",
                        
                        "TerminalID"    =>  isset($paymentResponse->PaymentResult->PaymentAcquirerData->AcquirerPOIID)?$paymentResponse->PaymentResult->PaymentAcquirerData->AcquirerPOIID:"",
                        
                        "BankDateTime"  =>  "",
                        "CardType"      =>  isset($paymentResponse->PaymentResult->PaymentInstrumentData->CardData->PaymentBrand)?$paymentResponse->PaymentResult->PaymentInstrumentData->CardData->PaymentBrand:"",
                        
                        "CardPAN"       =>  isset($paymentResponse->PaymentResult->PaymentInstrumentData->CardData->MaskedPan)?$paymentResponse->PaymentResult->PaymentInstrumentData->CardData->MaskedPan:"",
                        
                        "AuthCode"      =>  isset($paymentResponse->PaymentResult->PaymentAcquirerData->ApprovalCode)?$paymentResponse->PaymentResult->PaymentAcquirerData->ApprovalCode:"",
                        
                        "status"        =>  $paymentResponse->Response->Result=="Success"?"'SUCCESS'":"'FAILED'",//STATIC RESPONSE 
                        
                        "result"        =>  $paymentResponse->Response->Result=="Success"?true:false,
                        
                        "surcharge"     =>  0,
                        "Receipt"       =>  "",
                        "response"      =>  $response
                    ];
                    
                    $this->message_type="Payment was Settled!";
                    $this->response_code='201';
                    $this->result=true;
                    
                }else {
                    
                    $this->response_code='201';
                    $this->result=false;
                    $this->output = (object)[];
                    $this->message_type = $paymentResponse->Response->ErrorCondition;
                    return true;    
                }
                
            }else{
                
                $this->response_code='201';
                $this->result=false;
                $this->output = (object)[];
                $this->message_type = $response->SaleToPOIResponse->TransactionStatusResponse->Response->ErrorCondition;
                
            }
            
        }
        
        Log::info("Response >>>>>>>>>>>>>> ");
        Log::info(json_encode($this->output));
        
        return true;
    }

    public function query()
    {
        
        $header=[
            'Content-Type:application/json',
            'x-api-key:'.$this->partner_secret
        ];
        
        //echo $this->json_payload;die;
        
        $ch = curl_init($this->request_uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $this->api_response = curl_exec($ch);
        
        //echo $this->api_response;die;
        
        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
    }

    public function setPayload(){
        
        $qrcode = $this->request_data["qrcode"];
        $orderId = $this->request_data["order_id"];
        $this->unique_txn=$this->request_data["txn_identifier"];
        $tableNo = round($this->request_data["table_no"]);
        $amount = $this->request_data["total_amount"];
        $currency_code = $this->request_data["currency_code"];
        
        $payload=[];
        
        $date = date("Y-m-d");
        $time = date("H:i:s");
        
        $serviceId=time();
        
        $payload=[
            "SaleToPOIRequest"      => [
              "MessageHeader"       => [
                "ProtocolVersion"   => "3.0",
                "MessageClass"      => "Service",
                "MessageCategory"   => "TransactionStatus",
                "MessageType"       => "Request",
                "SaleID"            => $this->partner_id,
                "ServiceID"         => substr(uniqid(),3) ,
                "POIID"             => $this->device_id
              ],
              "TransactionStatusRequest"=> [
                "ReceiptReprintFlag"    => true,
                "DocumentQualifier"     => [
                  "CashierReceipt",
                  "CustomerReceipt"
                ],
                "MessageReference"=> [
                  "SaleID"          => $this->partner_id,
                  "ServiceID"       => substr($this->unique_txn, 2),
                  "MessageCategory" => "Payment"
                ]
              ]
            ]

        ];
        
        $this->payload =json_encode($payload);
        
        //echo $this->payload;die;
        
        $this->api_payload= json_encode($payload);
    }
    public function getPayload(){
        return $this->payload;
    }
    
    public function doRequest(){
        
        $this->json_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/json";

        $this->api_endpoint = $this->api_url;
        
        $this->request_uri = $this->api_endpoint;
        
        $this->query();
    }
    
}