<?php

namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Models\Transaction;

use Log;

/*
 * CREATED BY : Edwin Warming Gunawan
 * ON          : 11 August 2020
 * Adyen Api Service 
 * 
 */

class Reversal extends BaseWindcave
{
    
    public function process($data)
    {
        
        $this->op_type = "ORDER_REVERSAL";

        $this->request_data = $data;

        $this->getConfig();
        
        $status = "UNDEFINED";
        
        $this->setPayload("reversal");
        
        $this->getPayload();
        
        Log::info("Requested >>>>>>>>>>>>>>>>");
        Log::info(json_encode($this->getPayload()));
        
        
        $this->doRequest();
        
        if($this->handleResult() == false)
        {
          return false;
        }
        
        $response = json_decode($this->api_response);
        
        if(isset($response->SaleToPOIRequest->EventNotification)){ //NO RESPONSE OR EVENT FROM TERMINAL
            $this->http_code=201;
            $this->response_code=201;
            $find = array("+","message=");
            $this->message_type = str_replace($find," ",$response->SaleToPOIRequest->EventNotification->EventDetails);
            $this->result=false;
            
        }else{//any event from terminal
            
            $this->http_code=201;
            
            $reversalResponse=$response->SaleToPOIResponse->ReversalResponse;
            
            if(isset($reversalResponse)){
                
                if($reversalResponse->Response->Result=="Success"){
                    
                    $AdditionalResponse = $reversalResponse->Response->AdditionalResponse;
                   
                    $this->output = [
                        "ID"            =>  isset($reversalResponse->SaleData->SaleTransactionID->TransactionID)?$reversalResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "STAN"          =>  isset($reversalResponse->SaleData->SaleTransactionID->TransactionID)?$reversalResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "TxnRef"        =>  isset($reversalResponse->SaleData->SaleTransactionID->TransactionID)?$reversalResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "MerchantId"    =>  isset($reversalResponse->PaymentResult->PaymentAcquirerData->MerchantID)?$reversalResponse->PaymentResult->PaymentAcquirerData->MerchantID:"",
                        
                        "TerminalID"    =>  isset($reversalResponse->PaymentResult->PaymentAcquirerData->AcquirerPOIID)?$reversalResponse->PaymentResult->PaymentAcquirerData->AcquirerPOIID:"",
                        
                        "BankDateTime"  =>  "",
                        "CardType"      =>  isset($reversalResponse->PaymentResult->PaymentInstrumentData->CardData->PaymentBrand)?$reversalResponse->PaymentResult->PaymentInstrumentData->CardData->PaymentBrand:"",
                        
                        "CardPAN"       =>  isset($reversalResponse->PaymentResult->PaymentInstrumentData->CardData->MaskedPan)?$reversalResponse->PaymentResult->PaymentInstrumentData->CardData->MaskedPan:"",
                        
                        "AuthCode"      =>  isset($reversalResponse->PaymentResult->PaymentAcquirerData->ApprovalCode)?$reversalResponse->PaymentResult->PaymentAcquirerData->ApprovalCode:"",
                        
                        "status"        =>  $reversalResponse->Response->Result=="Success"?"'SUCCESS'":"'FAILED'",//STATIC RESPONSE 
                        
                        "result"        =>  $reversalResponse->Response->Result=="Success"?true:false,
                        "surcharge"     =>  0,
                        "Receipt"       =>  "",
                        "response"      =>  $response
                    ];
                    
                    $this->message_type="Reversal was Settled!";
                    $this->response_code='201';
                    $this->result=true;
                    
                }else if($response->SaleToPOIResponse->ReversalResponse->Response->Result=="Failure"){
                    $this->response_code='201';
                    $this->result=false;
                    $this->output = (object)[];
                    $this->message_type = $response->SaleToPOIResponse->ReversalResponse->Response->ErrorCondition;
                    return true;    
                }
            
            }
            
        }
        
        Log::info("Response >>>>>>>>>>>>>> ");
        Log::info(json_encode($this->output));
        
        return true;
    }

    public function query()
    {
        $header=[
            'Content-Type:application/json',
            'x-api-key:'.$this->partner_secret
        ];
        
        //echo $this->json_payload;die;
        
        $ch = curl_init($this->request_uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $this->api_response = curl_exec($ch);
        
        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
    }

    public function setPayload($type){
        $txn_id=explode("|",$this->txn_id);
        
        $payload=[];
        
        $date = date("Y-m-d");
        $time = date("H:i:s");
        
        switch($type){
            case "reversal":
                $payload=[
                    "SaleToPOIRequest"=>[
                        "MessageHeader"=>[
                            "ProtocolVersion" =>  "3.0",
                            "MessageClass"    =>  "Service",
                            "MessageCategory" =>  "Reversal",
                            "MessageType"     =>  "Request",
                            "SaleID"          =>  $this->partner_id,
                            "ServiceID"       =>  substr(uniqid(),3) ,
                            "POIID"           =>  $this->device_id
                        ],
                        "ReversalRequest"=>[
                                "OriginalPOITransaction"=>[
                                    "POITransactionID"=>[
                                        "TransactionID" =>  $txn_id[1],
                                        "TimeStamp"     =>  $date."T".$time."+00:00"
                                    ]
                                ],
                                "ReversalReason"=>"MerchantCancel"
                                   
                        ]
                    ]
                ];
                
                break;
            
            default :
                break;
        }
        
        $this->payload =json_encode($payload);
        //echo $this->payload;die;
        
        $this->api_payload= json_encode($payload);
    }
    public function getPayload(){
        return json_encode($this->payload);
    }
    
    public function doRequest(){
        
        $this->json_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/json";

        $this->api_endpoint = $this->api_url;
        
        $this->request_uri = $this->api_endpoint;
        
        $this->query();
    }
}