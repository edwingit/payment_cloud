<?php
  
namespace App\Api\v1\Services; 

use App\Api\v1\Services\Contract\BaseWindcave;

use App\Api\v1\Models\Transaction;

use Log;


/*
 * CREATED BY : Edwin Warming Gunawan
 * ON          : 22 June 2020
 * Adyen Api Service 
 * 
 */
class Purchase extends BaseWindcave
{
    private $payload; 
    private $unique_txn;
    private $Terminal_response;
    private $receipt;
    
    
    public function process($data)
    {
        //update 03102020
        
        $this->op_type = "PURCHASE_CREATE";

        $this->request_data = $data;

        $this->getConfig();
        
        $status = "UNDEFINED";
        
        $this->setPayload("purchase");
        
        $this->getPayload();
        
        Log::info("Requested >>>>>>>>>>>>>>>>");
        Log::info(json_encode($this->getPayload()));
        
        
        $this->doRequest();
        
        if($this->handleResult() == false)
        {
          return false;
        }
        
        $response = json_decode($this->api_response);
        
         
        //set map api url
        /*
        $url = "http://localhost/adyen/response-cancel.json";
        
        $json = file_get_contents($url);
        $response = json_decode($json);
        */
        
        if(isset($response->SaleToPOIRequest->EventNotification)){ //NO RESPONSE OR EVENT FROM TERMINAL
            $this->http_code=201;
            $this->response_code=201;
            $find = array("+","message=");
            $this->message_type = str_replace($find," ",$response->SaleToPOIRequest->EventNotification->EventDetails);
            $this->result=false;
            
        }else{//any event from terminal
            
            $this->http_code=201;
            
            $paymentResponse=$response->SaleToPOIResponse->PaymentResponse;
            
            if(isset($paymentResponse)){
                
                if($paymentResponse->Response->Result=="Success"){
                    
                    $AdditionalResponse = $paymentResponse->Response->AdditionalResponse;
                   
                    $this->output = [
                        "ID"            =>  isset($paymentResponse->SaleData->SaleTransactionID->TransactionID)?$paymentResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "STAN"          =>  isset($paymentResponse->SaleData->SaleTransactionID->TransactionID)?$paymentResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "TxnRef"        =>  isset($paymentResponse->SaleData->SaleTransactionID->TransactionID)?$paymentResponse->SaleData->SaleTransactionID->TransactionID:"",
                        
                        "MerchantId"    =>  isset($paymentResponse->PaymentResult->PaymentAcquirerData->MerchantID)?$paymentResponse->PaymentResult->PaymentAcquirerData->MerchantID:"",
                        
                        "TerminalID"    =>  isset($paymentResponse->PaymentResult->PaymentAcquirerData->AcquirerPOIID)?$paymentResponse->PaymentResult->PaymentAcquirerData->AcquirerPOIID:"",
                        
                        "BankDateTime"  =>  "",
                        "CardType"      =>  isset($paymentResponse->PaymentResult->PaymentInstrumentData->CardData->PaymentBrand)?$paymentResponse->PaymentResult->PaymentInstrumentData->CardData->PaymentBrand:"",
                        
                        "CardPAN"       =>  isset($paymentResponse->PaymentResult->PaymentInstrumentData->CardData->MaskedPan)?$paymentResponse->PaymentResult->PaymentInstrumentData->CardData->MaskedPan:"",
                        
                        "AuthCode"      =>  isset($paymentResponse->PaymentResult->PaymentAcquirerData->ApprovalCode)?$paymentResponse->PaymentResult->PaymentAcquirerData->ApprovalCode:"",
                        
                        "status"        =>  $paymentResponse->Response->Result=="Success"?"'SUCCESS'":"'FAILED'",//STATIC RESPONSE 
                        
                        "result"        =>  $paymentResponse->Response->Result=="Success"?true:false,
                        "surcharge"     =>  0,
                        "Receipt"       =>  "",
                        "response"      =>  $response
                    ];
                    
                    //save to transaction terminal table
                    
                    $TransactionID = $paymentResponse->POIData->POITransactionID->TransactionID;
                    
                    $transaction = new Transaction();
                    $transaction->saveData([
                        "order_id"      =>  $this->request_data["order_id"],
                        "customer_id"   =>  $this->request_data["customer_id"],
                        "total_amount"  =>  doubleval($this->request_data["total_amount"]),
                        "table_no"      =>  $this->request_data["table_no"],
                        "currency_code" =>  $this->request_data["currency_code"],
                        "msg_id"        =>  0,
                        "txn_id"        =>  $this->request_data["txn_identifier"]."|".$TransactionID,
                        "status"        =>  1,
                        "merchant_id"   =>  $this->request_data["merchant_id"],
                        "app_id"        =>  $this->request_data["mode"]=="kiosk-ios"?3:4,
                        "client_info"   =>  $this->request_data["client_info"]
                    ]);
                    
                    $this->message_type="Payment was Settled!";
                    $this->response_code='201';
                    $this->result=true;
                    
                }else if($response->SaleToPOIResponse->PaymentResponse->Response->Result=="Failure"){
                    $this->response_code='201';
                    $this->result=false;
                    $this->output = (object)[];
                    $this->message_type = $response->SaleToPOIResponse->PaymentResponse->Response->ErrorCondition;
                    return true;    
                }
            
            }
            
        }
        
        Log::info("Response >>>>>>>>>>>>>> ");
        Log::info(json_encode($this->output));
        
        return true;
    }

    public function query()
    {
        
        $header=[
            'Content-Type:application/json',
            'x-api-key:'.$this->partner_secret
        ];
        
        //echo $this->json_payload;die;
        
        $ch = curl_init($this->request_uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->http_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json_payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $this->api_response = curl_exec($ch);
        
        $this->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
    }

    public function setPayload($type){
        
        $qrcode = $this->request_data["qrcode"];
        $orderId = $this->request_data["order_id"];
        $this->unique_txn=$this->request_data["txn_identifier"];
        $tableNo = round($this->request_data["table_no"]);
        $amount = $this->request_data["total_amount"];
        $currency_code = $this->request_data["currency_code"];
        $subaccount = $this->subaccount;
        $commisions = $this->commisions;
        $tot_commisions = 0;
        $split_amount=0;
        
        //check whether this merchant has subaccount for splitting payment
        if($subaccount!=null){
            $tot_commisions = $amount - (($commisions/100) * $amount);
            $split_amount = $amount - $tot_commisions;
        }
        
        $payload=[];
        
        $date = date("Y-m-d");
        $time = date("H:i:s");
        
        $serviceId=time();
        
        switch($type){
            case "purchase":
                $payload=[
                    "SaleToPOIRequest"=>[
                        "MessageHeader"=>[
                            "ProtocolVersion" =>  "3.0",
                            "MessageClass"    =>  "Service",
                            "MessageCategory" =>  "Payment",
                            "MessageType"     =>  "Request",
                            "SaleID"          =>  $this->partner_id,
                            "ServiceID"       =>  substr($this->unique_txn, 2),
                            "POIID"           =>  $this->device_id
                        ],
                        "PaymentRequest"=>[
                                "SaleData"=>[
                                    "SaleTransactionID"=>[
                                        "TransactionID" =>  substr($this->unique_txn, 2),
                                        "TimeStamp"     =>  $date."T".$time."+00:00"
                                    ]
                                ],
                                "PaymentTransaction"=>[
                                    "AmountsReq"=>[
                                        "Currency"          =>  $currency_code,
                                        "RequestedAmount"   =>  doubleval($amount)
                                    ]
                                ]
                        ]
                    ]
                ];
                
                if($qrcode && $qrcode!=""){
                    $qr= ["AllowedPaymentBrand"   =>[
                            $qrcode
                        ]];
                    
                    $payload["SaleToPOIRequest"]["PaymentRequest"]["PaymentTransaction"]["TransactionConditions"]=$qr;
                    
                }
                
                if($subaccount != null){
                    $splits=[
                        [
                            "amount"    =>[
                                "value" => number_format($split_amount,2)
                            ],
                            "type"      =>  "MarketPlace",
                            "account"   =>  $this->subaccount,
                            "reference" =>  substr($this->unique_txn, 2)
                        ],
                        [
                            "amount"=>[
                                "value"=>number_format($tot_commisions,2)
                            ],
                            "type"      =>  "Commission",
                            "reference" =>  substr($this->unique_txn, 2)
                        ]
                      ];  
                    
                    $payload["SaleToPOIRequest"]["PaymentRequest"]["splits"]=$splits;
                    
                }
                
                break;
            
            case "status":
                $payload=[
                    "SaleToPOIRequest"      => [
                      "MessageHeader"       => [
                        "ProtocolVersion"   => "3.0",
                        "MessageClass"      => "Service",
                        "MessageCategory"   => "TransactionStatus",
                        "MessageType"       => "Request",
                        "SaleID"            => $this->partner_id,
                        "ServiceID"         => substr(uniqid(),3) ,
                        "POIID"             => $this->device_id
                      ],
                      "TransactionStatusRequest"=> [
                        "ReceiptReprintFlag"    => true,
                        "DocumentQualifier"     => [
                          "CashierReceipt",
                          "CustomerReceipt"
                        ],
                        "MessageReference"=> [
                          "SaleID"          => $this->partner_id,
                          "ServiceID"       => substr($this->unique_txn, 2),
                          "MessageCategory" => "Payment"
                        ]
                      ]
                    ]
                    
                ];
               
                break;
            
            default :
                break;
        }
        
        $this->payload =json_encode($payload);
        
        //echo $this->payload;die;
        
        $this->api_payload= json_encode($payload);
    }
    public function getPayload(){
        return json_encode($this->payload);
    }
    
    public function doRequest(){
        
        $this->json_payload = $this->payload;

        $this->http_method = "POST";  

        $this->application_type = "application/json";

        $this->api_endpoint = $this->api_url;
        
        $this->request_uri = $this->api_endpoint;
        
        $this->query();
    }
    
}