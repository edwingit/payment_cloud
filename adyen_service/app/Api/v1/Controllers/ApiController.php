<?php

    namespace App\Api\v1\Controllers;

    use Illuminate\Http\Request;

    use App;

    use App\Library\Bases\BaseApiController;
    use App\Api\v1\Requests\Repository;
    use App\Api\v1\Processors\Processor;
    
    use Format;
    use Log;
    class ApiController extends BaseApiController
    {
        public function postPaymentOrder(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $service = app()->make("api");   
            $form_processor->setOperation("order");
            return $this->startProcess($request, $form_processor, $data_processor, $service);
        }

        public function postPaymentQuery(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("query");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }

        public function postPaymentReversal(Request $request, Repository $form_processor, Processor $data_processor)
        {
            
            $model = app()->make("api");
            $form_processor->setOperation("reversal");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }

        public function postPaymentRefund(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("refund");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }

        public function postPaymentPurchase(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("purchase");
            
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }
        
        public function postPaymentStatus(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("status");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }
        
        public function postPaymentReceipt(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("receipt");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }

        public function getNotification(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("get_notification");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }

        public function postDownloadLog(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("get_logs");

            if($form_processor->validate($request) == false)
            {
                return Format::apiResponse("DATA_ERR", "Invalid Data", $form_processor->getBaseErrors(), [], 400);
            }

            if($data_processor->process($form_processor->getCleanData($request), $model) == false)
            {
                return $data_processor->getError();
            }
            
            return response()->download($data_processor->getOutput());
        }

        public function postDownloadReport(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("get_report");

            if($data_processor->process($form_processor->getCleanData($request), $model) == false)
            {
                return $data_processor->getError();
            }
            
            return response()->download($data_processor->getOutput());
        }

        public function postUpdatePayment(Request $request, Repository $form_processor, Processor $data_processor)
        {
            $model = app()->make("api");
            $form_processor->setOperation("update_status");
            return $this->startProcess($request, $form_processor, $data_processor, $model);
        }
       
    }