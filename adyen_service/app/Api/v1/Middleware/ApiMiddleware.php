<?php

    namespace App\Api\v1\Middleware;

    use Closure;
    use Response;

    use App\Library\Bases\BaseEmptyData;
    use App\Library\Constants\ApiMessage;
    use App\Library\Constants\ApiCode;

    use Format;

    class ApiMiddleware
    {
        public function handle($request, Closure $next)
        {
            $service = app()->make("api");

            $data = json_decode($request->getContent(), true);

            /*
            $secret_key = $request->header('X-AUTHORIZATION');

            if(!$secret_key)
            {
                return Format::apiResponse(ApiCode::AUTH_ERR, ApiMessage::AUTH_ERR, [ ["secret_key" => 'empty'] ], new BaseEmptyData(), 401);
            }

            $account_data = $service->getAccount($secret_key);

            if($account_data->isEmpty())
            {
                return Format::apiResponse(ApiCode::AUTH_ERR, ApiMessage::AUTH_ERR, [ ["account" => 'not found'] ], new BaseEmptyData(), 401);
            }

            $data = json_decode($request->getContent(), true);

            if(!isset($data["merchant_id"]) || !$data["merchant_id"]) 
            {
                return Format::apiResponse(ApiCode::AUTH_ERR, ApiMessage::AUTH_ERR, [ ["account" => 'not found'] ], new BaseEmptyData(), 401);
            }

            $check_config = $service->checkConfig($data["merchant_id"]);

            if(!$check_config)
            {
                return Format::apiResponse(ApiCode::OUT_ERR, ApiMessage::OUT_ERR, [ ["config" => "not found"] ], new BaseEmptyData(), 404);
            }
    
            // we need this data to be passed to controller
            // $request->app_id = $account_data[0]["id"];
            */
            
            return $next($request);
        }
    }
