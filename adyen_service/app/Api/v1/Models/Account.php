<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Account extends Model
    {
        public $table = 'accounts';
        public $timestamps = false;

        public function getAccount($secret_key)
        {
            return $this->where("secret_key", $secret_key)->where("is_active", 1)->get();
        }
    }