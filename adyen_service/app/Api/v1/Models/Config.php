<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Config extends Model
    {
        public $table = 'payment_settings';
        public $timestamps = false;
        
        public function getAll($merchant_id)
        {
            
            //return $this->where("merchant_id", $merchant_id)->where("is_active", 1)->get();
            return $this->where("restaurant_id", $merchant_id)->get();
        }
        
        public function getByTerminal($merchant_id,$terminal)
        {
            return $this->where("restaurant_id", $merchant_id)->where("terminal", $terminal)->take(1)->get();
        }
    }