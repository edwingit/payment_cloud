<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Merchants extends Model
    {
        public $table = 'users';
        public $timestamps = false;
       
        public function getAccount($merchant_key)
        {
            return $this->where("customer_key", $merchant_key)->take(1)->get();
        }
    }