<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Users extends Model
    {
        public $table = 'users_extendeds';
        public $timestamps = false;
       
        public function getSubAccount($merchant_id)
        {
            return $this->where("id", $merchant_id)->take(1)->get();
        }
    }