<?php 

    namespace App\Api\v1\Models;

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class AppPermission extends Model
    {
        public $table = 'app_permissions';
        public $timestamps = false;

        public function getAppPermission($app_id, $app_key, $app_secret, $status)
        {
            
            return $this->where("app_id", $app_id)
                        ->where("app_key", $app_key)
                        ->where("app_secret", $app_secret)
                        ->where("is_active", $status)
                        ->get();
        }
    }