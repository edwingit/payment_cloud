<?php 

    namespace App\Api\v1\Models; 

    use Illuminate\Database\Eloquent\Model;

    use DB;
    
    class Transaction extends Model
    {
        public $table = "transactions_terminal";
        public $timestamps = false;

        public function saveData(array $data)
        {
            $this->order_id = $data["order_id"];
            $this->customer_id = $data["customer_id"];
            $this->total_amount = $data["total_amount"];
            $this->table_no = $data["table_no"];
            $this->currency_code = $data["currency_code"];
            $this->msg_id = $data["msg_id"];
            $this->txn_id = $data["txn_id"];
            $this->status = $data["status"];;
            $this->merchant_id = $data["merchant_id"];
            $this->app_id = $data["app_id"];
            $this->client_info = $data["client_info"];
            $this->created_date = date("Y-m-d h:i:s");
            $this->updated_date = date("Y-m-d h:i:s");
            $this->save();

            return $this->id;
        }

        public function getTransactionByTxnId($txn_id,$merchant_id)
        {
            
            return $this->where("txn_id", $txn_id)
                        ->where("merchant_id", $merchant_id)
                        ->get();
        }
        
        public function getTransactionForReversal($merchant_id,$order_id,$total_amount)
        {
            return $this->where("merchant_id", $merchant_id)
                        ->where("order_id", $order_id)
                        ->where("total_amount", $total_amount)
                        ->get();
        }
    }