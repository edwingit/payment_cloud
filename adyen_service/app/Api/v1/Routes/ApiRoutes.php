<?php
    
    namespace App\Api\v1\Routes;

    use Laravel\Lumen\Application;

    use App\Library\Bases\BaseModuleRoute;

    class ApiRoutes extends BaseModuleRoute
    {
        public function __construct()
        {
            $this->controller_ns = 'App\Api\\v1\Controllers';
            $this->route_prefix = BaseModuleRoute::V1_GLOBAL_PREFIX;
        }

        public function bind(Application $app)
        {
            
            $app->router->group(['prefix' => $this->route_prefix, 'namespace' => $this->controller_ns], function () use ($app) {
                
                $app->router->post('/order', [
                    "uses" => "ApiController@postPaymentOrder",
                ]);

                $app->router->post("/query", [
                    "uses" => "ApiController@postPaymentQuery",
                ]);

                $app->router->post("/reversal", [
                    "uses" => "ApiController@postPaymentReversal",
                ]);

                $app->router->post("/refund", [
                    "uses" => "ApiController@postPaymentRefund",
                ]);

                $app->router->get("/notification", [
                    "uses" => "ApiController@getNotification",
                ]);

                $app->router->post("/logs", [
                    "uses" => "ApiController@postDownloadLog",
                ]);

                $app->router->post("/report", [
                    "uses" => "ApiController@postDownloadReport",
                ]);

                //start windcave
                $app->router->post('/purchase', [
                    "uses" => "ApiController@postPaymentPurchase",
                ]);
                
                $app->router->post("/status", [
                    "uses" => "ApiController@postPaymentStatus",
                ]);
                $app->router->post("/receipt", [
                    "uses" => "ApiController@postPaymentReceipt",
                ]);
                
            });
        }
    }