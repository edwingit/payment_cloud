<?php

    namespace App\Api\v1\Events;

    use Illuminate\Queue\SerializesModels;

    use App\Library\Bases\LogEventInterface;

    class AuthLogEvent implements LogEventInterface
    {
        use SerializesModels;
        
        public $origin_ip;
        public $origin_merchant_id;
        public $app_id;
        public $auth_header;
        public $message_type;
        public $response_code;
        
        public function __construct($input)
        {
            $this->origin_ip = $input["origin_ip"];
            $this->origin_merchant_id = $input["merchant_id"];
            $this->app_id = $input["app_id"];
            $this->auth_header = $input["auth_header"];
            $this->message_type = $input["message_type"];
            $this->response_code = $input["response_code"];
        }
    }