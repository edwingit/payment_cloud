<?php

    namespace App\Api\v1\Providers;

    use Illuminate\Support\ServiceProvider;
    use Illuminate\Http\Request;

    use App\Api\v1\Services\Windcave;

    class ApiServiceProvider extends ServiceProvider
    {
        protected $defer = true;

        public function boot(Request $request)
        {
            
        }

        public function register()
        {
            $this->app->bind("api", function ($app)
            {
                return new Windcave();
            });
        }

        public function provides()
        {
            return ["api"];
        }
    }
