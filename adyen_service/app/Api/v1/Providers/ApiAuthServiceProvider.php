<?php

    namespace App\Api\v1\Providers;

    use Illuminate\Support\ServiceProvider;
    use Illuminate\Http\Request;

    use App\Api\v1\Requests\Repository;

    use App\Library\Helpers\ApiAuth;
    
    use App\Library\Constants\ApiMessage;
    use App\Library\Constants\ApiCode;

    use App\Library\Bases\BaseEmptyData;

    use Format;

    class ApiAuthServiceProvider extends ServiceProvider
    {
        protected $defer = true;

        public function register()
        {
            $this->app->bind("api_auth", function ($app) {
                $repository = new Repository();
                
                $repository->validate($app->request);
                
                $data = $repository->getData();
                
                $auth_helper = new ApiAuth($data["auth_header"], $data["merchant_id"], $data["mode"]);
                
                return $auth_helper;
            });
        }

        public function provides()
        {
            return ["api_auth"];
        }
    }
