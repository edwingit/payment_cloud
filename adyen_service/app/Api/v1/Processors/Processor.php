<?php
	
namespace App\Api\v1\Processors;

use App\Library\Bases\BaseProcessor;

class Processor extends BaseProcessor
{
    public function process($data, $model)
    {
        
        try
        {
            switch($data["op_type"])
            {
                case "order":
                    $this->output = $model->makePayment($data);
                    break;
                case "purchase":
                    $this->output = $model->makePurchase($data);
                    break;
                case "query":
                    $this->output = $model->getPayments($data);
                    break;
                case "reversal":
                    
                    $this->output = $model->makePaymentReversal($data);
                    
                    break;

                case "refund":
                    $this->output = $model->makePaymentRefund($data);
                    break;

                case "update_status":
                    $this->output = $model->updateStatus($data);
                    break;

                case "get_notification":
                    $this->output = $model->getNotification($data);
                    break;

                case "get_logs":
                    $this->output = $model->getLogs($data);
                    break;

                case "get_report":
                    $this->output = $model->getReport($data);
                    break;
                case "status":
                    $this->output = $model->getStatus($data);
                    break;
                case "receipt":
                    $this->output = $model->getReceipt($data);
                    break;
            }

            $this->response_code = $model->getResponseCode();
            $this->message_type = $model->getMessageType();
            $this->http_code = $model->getHttpCode();
            $this->result = $model->getResult();
           
            return true;
        }
        catch(\Exception $e)
        {
            
            $this->response_code = $model->getResponseCode() ? $model->getResponseCode() : 1000;
            $this->message_type = $model->getMessageType() ? $model->getMessageType() : "SYS_ERR";
            $this->http_code = $model->getHttpCode() ? $model->getHttpCode() : 500;
            $this->error_messages = $model->getErrorMessages() ? $model->getErrorMessages() : [ [ "sys_error" => $e->getMessage() ] ];
            \Log::error($e->getMessage() . " at " . $e->getFile() . " in line " . $e->getLine());

            return false;
        }

    }
}