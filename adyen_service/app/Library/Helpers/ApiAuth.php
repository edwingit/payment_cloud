<?php

    namespace App\Library\Helpers;

    use App\Api\v1\Models\Apps;
    use App\Api\v1\Models\AppPermission;
    use App\Api\v1\Models\Payment;

    use App\Library\Constants\ApiMessage;
    use App\Library\Constants\ApiCode;

    use App\Library\Helpers\PaymentHelper;

    use App\Api\v1\Events\AuthLogEvent;

    class ApiAuth 
    {
        private $auth_header;
        public $app_id;
        private $app_key;
        private $app_secret;
        private $merchant_id;
        private $response_code;
        private $message_type;
        private $http_code;
        private $error_message;
        private $app_permission;

        public function __construct($auth_header, $merchant_id, $app_name)
        {
            $this->auth_header = $auth_header;
            $this->merchant_id = $merchant_id;
            $this->app_name = $app_name;
        }

        public function authenticate()
        {
            $this->getAppId();
            
            // check if auth header is a valid base64 string
            if(!PaymentHelper::is_base64($this->auth_header))
            {
                $this->setResponse(ApiCode::AUTH_BAD_FORMAT, ApiMessage::AUTH_BAD_FORMAT, 400, [ ["auth_key" => "bad format"] ]);

                event(new AuthLogEvent($this->logData()));

                return false;
            }

            $this->app_key = "";
            $this->app_secret = "";

            $auth_header = base64_decode($this->auth_header);
            $auth_header = explode(":", $auth_header);
            
            // check if auth format is api_key:api_secret combination
            if(count($auth_header) < 2)
            {
                $this->setResponse(ApiCode::AUTH_BAD_LENGTH, ApiMessage::AUTH_BAD_LENGTH, 400, [ ["auth_key" => "bad format"] ]);

                event(new AuthLogEvent($this->logData()));

                return false;
            }

            $this->app_key = $auth_header[0];
            $this->app_secret = $auth_header[1];

            $this->getAppPermission();
            
            if(!$this->app_permission)
            {
                $this->setResponse(ApiCode::AUTH_NO_PERMISSION, ApiMessage::AUTH_NO_PERMISSION, 401, [ ["app_permission" => "no content"] ]);

                event(new AuthLogEvent($this->logData()));

                return false;
            }

            $this->setResponse(ApiCode::AUTH_SUCCESS, ApiMessage::AUTH_SUCCESS, 200, []);

            event(new AuthLogEvent($this->logData()));

            return true;
        }

        private function getAppId()
        {
            $apps_model = new Apps();
            $app_data = $apps_model->getByName($this->app_name, $status = 1);
            
            if(!$app_data->isEmpty())
            {
                $this->app_id = $app_data[0]["id"];
            }
            
        }

        private function getAppPermission()
        {
            $app_permission_model = new AppPermission();
            $app_permission_data = $app_permission_model->getAppPermission($this->app_id, $this->app_key, $this->app_secret, $status = 1);
            
            if(!$app_permission_data->isEmpty())
            {
                $this->app_permission = $app_permission_data[0]["id"];
            }
            
        }

        public function getHttpCode()
        {
            return $this->http_code;
        }

        public function getErrorMessage()
        {
            return $this->error_message;
        }

        public function getResponseCode()
        {
            return $this->response_code;
        }

        public function getMessageType()
        {
            return $this->message_type;
        }

        private function setResponse($response_code, $message_type, $http_code, $message)
        {
            $this->response_code = $response_code;
            $this->message_type = $message_type;
            $this->http_code = $http_code;
            $this->error_message = $message;
        }

        protected function logData()
        {
            return [
                "origin_ip" => app()->request->ip(),
                "merchant_id" => $this->merchant_id,
                "app_id" => $this->app_id,
                "auth_header" => $this->auth_header,
                "message_type" => $this->message_type,
                "response_code" => $this->response_code,
            ];
        }
    }
