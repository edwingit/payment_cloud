<?php

    namespace App\Library\Helpers;

    use File;

    class PaymentHelper 
    {
        public static function createRootFolder() 
        {
            $path = storage_path("logs/payments");

            if (!File::exists($path)) {
                File::makeDirectory($path);
            }
        }

        public static function createParentFolder($folder_name) 
        {
            $path = storage_path("logs/payments/" . $folder_name);

            if (!File::exists($path)) 
            {
                File::makeDirectory($path);
            }
        }

        public static function createMerchantFolder($folder_name, $merchant_key) 
        {
            $path = storage_path("logs/payments/" . $folder_name . '/' . $merchant_key);

            if (!File::exists($path)) 
            {
                File::makeDirectory($path);
            }
        }

        public static function createMerchantFolderByDate($folder_name, $merchant_key) 
        {
            $log_date = date("Y-m-d", time());

            $path = storage_path("logs/payments/" . $folder_name . "/" . $merchant_key . "/" . $log_date);

            if (!File::exists($path)) 
            {
                File::makeDirectory($path);
            }
        }

        public static function saveLogToFile($log_data, $filename) 
        {
            $contents = "";

            foreach ($log_data as $log) 
            {
                $contents .= "Transaction Date: " . $log->txn_date;
                $contents .= PHP_EOL;

                switch ($log->type) {
                    case 1:
                        $log_type = "ORDER_CREATE";
                        break;

                    case 2:
                        $log_type = "ORDER_REVERSAL";
                        break;

                    case 3:
                        $log_type = "ORDER_REFUND";
                        break;

                    case 4:
                        $log_type = "ORDER_QUERY";
                        break;
                }

                $contents .= "Operation: " . $log_type;
                $contents .= PHP_EOL;

                $contents .= "Order ID: " . $log->order_id;
                $contents .= PHP_EOL;

                $contents .= "Order Amount: " . $log->total_amount;
                $contents .= PHP_EOL;

                $contents .= "Table No: " . $log->table_no;
                $contents .= PHP_EOL;

                $contents .= PHP_EOL;

                $request = json_decode($log->request);
                $response = json_decode($log->response);

                $contents .= "API Payload: ";
                $contents .= PHP_EOL;
                $contents .= json_encode($request, JSON_PRETTY_PRINT);

                $contents .= PHP_EOL;
                $contents .= PHP_EOL;

                $contents .= "API Response: ";
                $contents .= PHP_EOL;
                $contents .= json_encode($response, JSON_PRETTY_PRINT);

                $contents .= PHP_EOL;
                $contents .= PHP_EOL;

                $contents .= '----------------------------------------------------------------------------------------------------------------------------------------------';

                $contents .= PHP_EOL;
                $contents .= PHP_EOL;
            }

            $result = \Storage::disk("payment_log")->put($filename, $contents);

            return $result;
        }

        public static function saveReportToCsv($log_data, $filename) 
        {
            $contents = '';

            $csv_data = [];

            $public_path = base_path('public');

            $path = $public_path . "/" . $filename;
            $file = fopen($path, "w");

            fputcsv($file, ["Transaction Date", "Order ID", "Terminal", "Total Amount", "Payment Status", "Response Code"]);

            foreach ($log_data as $log) 
            {
                fputcsv($file, $log);
            }

            fclose($file);

            return true;
        }

        public static function is_base64($string)
        {
            // Check if there are valid base64 characters
            if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string)) 
            {
                return false;
            }

            // Decode the string in strict mode and check the results
            $decoded = base64_decode($string, true);
            
            if(false === $decoded) 
            {
                return false;
            }

            // Encode the string again
            if(base64_encode($decoded) != $string) 
            {
                return false;
            }

            return true;
        }

        public static function securityKey($action, $string)
        {
            $output = '';
            $encrypt_method = "AES-256-CBC";
            $key = env('SECRET');
            $iv = env('IV');

            if($action == 'encrypt') 
            {
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
            }
            
            if($action == 'decrypt')
            {
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }

            return $output;
        }
    }
