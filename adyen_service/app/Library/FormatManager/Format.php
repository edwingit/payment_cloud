<?php
    
    namespace App\Library\FormatManager;

    use Illuminate\Http\JsonResponse;

	class Format
	{
            public function apiResponse($response_code, $response_msg, $errors, $data, $http_code,$result=true)
            {
                
                return new JsonResponse([
                    'response_code' => $response_code,
                    'response_msg'  => $response_msg,
                    'errors'        => $errors,
                    'data'          => $data,
                    'message'       =>  $response_msg,
                    'nextCommand'   =>  '',
                    'result'        =>  $response_code!=200 && $response_code!=201?false:boolval($result)
                ], $http_code);
            }

            public static function ioObject($properties_name, $data, $singular = false)
            {
                $obj = new \stdClass();

                if($data)
                {
                    if($singular)
                    {
                        foreach($data as $datas)
                        {
                            $result = $datas;
                        }

                        $obj->{$properties_name} = $result;
                    }
                    else
                    {
                        $obj->{$properties_name} = $data;
                    }

                }
                else
                {
                    if($singular)
                    {
                        $obj->{$properties_name} = NULL;
                    }
                    else
                    {
                        $obj->{$properties_name} = $data;
                    }
                }

                return $obj;
            }
	}