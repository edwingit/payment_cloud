<?php
	
namespace App\Library\Constants;

class ApiCode
{
    const AUTH_SUCCESS = 9800;

    // auth header is not base64
    const AUTH_BAD_FORMAT = 9877;

    // is not in the format of api_key:api_secret
    const AUTH_BAD_LENGTH = 9899;

    // no permission
    const AUTH_NO_PERMISSION = 9900;

    const OP_SUCCESS = 2000;

    const AUTH_ERR = 3000;

    // invalid api request
    const IN_ERR = 7000;

    // data not found in our db
    const OUT_ERR = 7001;

    // grab auth error
    const GRAB_AUTH_ERR = 8000;

    // data not found in grab
    const GRAB_OUT_ERR = 8001;

    // invalid grab request
    const GRAB_IN_ERR = 8002;
}