<?php
	
namespace App\Library\Constants;

class ApiMessage
{
    const AUTH_SUCCESS = "AUTH_OK";
    const AUTH_BAD_FORMAT = "ERR_FORM";
    const AUTH_BAD_LENGTH = "ERR_LEN";
    const AUTH_NO_PERMISSION = "ERR_PERM";
    const IN_ERR = "IN_ERR";
    const OUT_ERR = "OUT_ERR";
    const GRAB_AUTH_ERR = "GRAB_AUTH_ERR";
    const GRAB_OUT_ERR = "GRAB_OUT_ERR";
    const GRAB_IN_ERR = "GRAB_IN_ERR";
    const AUTH_ERR = "AUTH_ERR";
}