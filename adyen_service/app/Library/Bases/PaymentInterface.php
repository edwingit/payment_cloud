<?php

namespace App\Library\Bases;

interface PaymentInterface
{
        public function makePayment($data);
        public function makePurchase($data);
        public function getPayments($data);
        public function makePaymentReversal($data);
        public function makePaymentRefund($data);
        public function updateStatus($data);
        public function getMessageType();
        public function getErrorMessages();
        public function getResponseCode();
        public function getNotification($data);
        public function checkConfig($user_id,$terminal);
        public function getLogs($data);
        public function getReport($data);
        public function getAccount($secret_key);
        public function getStatus($data);
        public function getReceipt($data);
        public function getResult();
}