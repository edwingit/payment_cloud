<?php

namespace App\Library\Bases;

use App\Http\Controllers\Controller;

use App\Events\ApiErrorLog;

use App\Library\Bases\BaseEmptyData;
use App\Library\Constants\ApiMessage;
use App\Library\Constants\ApiCode;

use Format;

class BaseApiController extends Controller
{
    protected function startProcess($input, $form_processor, $data_processor, $model)
    {
        if($form_processor->validate($input) == false)
        {
            return Format::apiResponse(ApiCode::IN_ERR, ApiMessage::IN_ERR, $form_processor->getBaseErrors(), new BaseEmptyData(), 400);
        }
        
        if($form_processor->getData()["merchant_id"]!=""){
            
            $config = $model->checkConfig($form_processor->getData()["merchant_id"],$form_processor->getData()["table_no"]);
            
            if(!$config)
            {
                
                return Format::apiResponse(ApiCode::OUT_ERR, ApiMessage::OUT_ERR, [ ["config" => "not found"] ], new BaseEmptyData(), 404);
            }
            
        }else{//end point reversal
            
            $customer_key=$form_processor->getData()["merchant_key"];
            $order_id=$form_processor->getData()["order_id"];
            $total_amount=$form_processor->getData()["total_amount"];
            $txn_identifier=$form_processor->getData()["txn_identifier"];
            
            $foundData = $model->foundReversal($customer_key,$order_id,$total_amount);
            
            if(!$foundData)
            {
                
                return Format::apiResponse(ApiCode::OUT_ERR, ApiMessage::OUT_ERR, [ ["config" => "not found"] ], new BaseEmptyData(), 404);
            }
            
        }
        
        $api_auth = app()->make("api_auth");
        
        if($api_auth->authenticate() == false)
        {
            
            return Format::apiResponse($api_auth->getResponseCode(), $api_auth->getMessageType(), $api_auth->getErrorMessage(), new BaseEmptyData(), $api_auth->getHttpCode());
        }

        $form_processor->setData("app_id", $api_auth->app_id);
        
        if($data_processor->process($form_processor->getData(), $model) == false)
        {
            
            return Format::apiResponse($data_processor->getResponseCode(), $data_processor->getMessageType(), $data_processor->getErrorMessages(), new BaseEmptyData(), $data_processor->getHttpCode());
        }
        
        return Format::apiResponse($data_processor->getResponseCode(), $data_processor->getMessageType(), [], $data_processor->getOutput(), $data_processor->getHttpCode(),$data_processor->getResult());
    }
}
