<?php
	
namespace App\Library\Bases;

use Validator;

abstract class BaseRepository
{
    protected $data;
    protected $rules;
    protected $errors;
    protected $operation_type;
    protected $json_data;
    protected $request;

    abstract public function setOperation($operation_type);

    abstract public function getInput();

    abstract public function setValidationData();

    abstract public function setValidationRules();

    public function decodeJsonData()
    {
        $this->json_data = json_decode($this->request->getContent(), true);
    }

    public function validate($request)
    {
        $this->request = $request;

        $this->decodeJsonData();
        $this->getInput();
        $this->setValidationData();
        $this->setValidationRules();

        $validation = Validator::make($this->data, $this->rules);
        
        if($validation->fails())
        {
            $this->errors = $validation->messages()->toJson();
            return false;
        }

        return true;
    }

    public function getBaseErrors()
    {
        $errors = json_decode($this->errors, true);

        $output = [];

        $i = 0;

        foreach($errors as $key => $error_messages)
        {
            $output[][$key] = implode(", ", $error_messages);
        }

        return $output;
    }

    public function getApiErrors()
    {
        $errors = [];
        $messages = $this->errors->getMessages();

        foreach($messages as $key => $value)
        {
            foreach ($messages[$key] as $msg_key => $msg_value) 
            {
                array_push($errors, $msg_value);
            }
        }

        $output["messages"] = $errors;

        return $output;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getCleanData($request)
    {
        $this->request = $request;

        $this->decodeJsonData();
        $this->getInput();
        $this->setValidationData();

        return $this->data;
    }

    public function setData($key, $value)
    {
        $this->data[$key] = $value;
    }
}