<?php

namespace App;

use Laravel\Lumen\Application as LumenApplication;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Application extends LumenApplication
{
	protected function registerLogBindings()
    {
        $this->singleton('Psr\Log\LoggerInterface', function () {
            return new Logger('lumen', $this->getMonologHandler());
        });
    }

    protected function getMonologHandler()
    {
    	$handlers = [];
    	$date=date('Y-m-d');
        $handlers[] = (new StreamHandler(storage_path("logs/tabs_stag_lumen-".$date.".log"), Logger::DEBUG))->setFormatter(new LineFormatter(null, null, true, true));
    
        return $handlers;
    }
}