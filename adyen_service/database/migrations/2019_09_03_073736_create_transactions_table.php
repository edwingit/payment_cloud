<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_terminal', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('order_id')->nullable(true)->default(0);
            $table->integer('customer_id')->nullable(true)->default(0);
            $table->decimal('total_amount', 10, 2)->nullable(true);
            $table->string('table_no', 255)->nullable(true);
            $table->string('currency_code', 255)->nullable(true);
            $table->text('msg_id')->nullable(true);
            $table->string('txn_id', 255)->nullable(true);
            $table->tinyInteger('status')->nullable(true)->comment('0 - Unpaid, 1 - Paid, 2 - Reversed');
            $table->integer('merchant_id')->nullable(true);
            $table->text('client_info')->nullable(true);
            $table->tinyInteger('app_id')->nullable(true);
            $table->dateTime('created_date')->nullable(true);
            $table->dateTime('updated_date')->nullable(true);
            $table->index('order_id');
            $table->index('merchant_id');
            $table->index('status');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
