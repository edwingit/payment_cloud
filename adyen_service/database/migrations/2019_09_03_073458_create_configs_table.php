<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('partner_id', 255)->nullable(false);
            $table->string('partner_secret', 255)->nullable(false);
            $table->string('station_id', 255)->nullable(false);
            $table->string('device_id', 255)->nullable(false);
            $table->string('vendor_id', 255)->nullable(false);
            $table->string('pos_name', 255)->nullable(false);
            $table->string('pos_version', 255)->nullable(false);
            $table->string('terminal_id', 255)->nullable(false);
            $table->string('api_url', 255)->nullable(false);
            $table->string('currency', 255)->nullable(false);
            $table->string('timezone', 255)->nullable(false);
            $table->tinyInteger('is_active')->nullable()->default(0);
            $table->integer('merchant_id')->nullable(false);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable(false);
            $table->index('merchant_id');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
