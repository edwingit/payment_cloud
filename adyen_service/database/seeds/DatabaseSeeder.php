<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call('app_permissions_seeder');
        $this->call('apps_seeder');
        $this->call('configs_seeder');
        Model::reguard();
        $this->command->info('Seeder done Bro!!!!');
        
    }
}
