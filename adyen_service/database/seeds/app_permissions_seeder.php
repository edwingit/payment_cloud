<?php

use Flynsarmy\CsvSeeder\CsvSeeder;

class app_permissions_seeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'app_permissions';
        $this->filename = base_path().'/database/seeds/csv/app_permissions.csv';
    }
    public function run()
    {
        
        DB::disableQueryLog();

        DB::table($this->table)->truncate();

        parent::run();
    }
}
