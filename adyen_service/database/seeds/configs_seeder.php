<?php

use Flynsarmy\CsvSeeder\CsvSeeder;


class configs_seeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'configs';
        $this->csv_delimiter = ',';
        $this->filename = base_path().'/database/seeds/csv/configs.csv';
    }
    public function run()
    {
        
        DB::disableQueryLog();

        DB::table($this->table)->truncate();

        parent::run();
    }
}
